import { useCallback, useMemo } from "react";
import { TableCellProps, TableHeaderProps } from "react-virtualized";
import styled, { useTheme } from "styled-components";

// Constants
export const ROW_HEIGHT = 80;
export const HEADER_HEIGHT = 30;

export const middleColumnStyles = {
  headerStyle: {
    marginLeft: 100,
    marginRight: 100,
  },
  style: {
    marginLeft: 100,
    marginRight: 100,
  },
};

export const endColumnStyles = {
  style: { display: "flex", justifyContent: "end" },
  headerStyle: { display: "flex", justifyContent: "end" },
};

// Table CSS
export const TableWrapper = styled.div<{
  height?: number;
  maxHeight?: string;
  showBottomBorder?: boolean;
}>`
  height: ${(p) => `${p.height}px`};
  max-height: ${(p) => (p.maxHeight ? p.maxHeight : "none")};
  width: 100%;
  overflow: auto;
  border-bottom: ${(p) =>
    p.showBottomBorder ? `1px solid ${p.theme.detailBlockBorder}` : "none"};
`;

export const TableTitle = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: #110f28;
  margin: 10px 0 25px;
  display: inline-block;
`;

export const HeadItem = styled.span`
  display: flex;
  align-items: center;
  cursor: pointer;
  font-size: 12.8px;
  font-weight: 600;
  color: ${(p) => p.theme.tableHeader};
  position: relative;
  white-space: nowrap;
  text-transform: none;
`;

export const CellItem = styled.span`
  font-size: 15px;
  color: ${(p) => p.theme.titleText};
`;

export const NoRowsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  gap: 10px;
`;

export const NoRowsRow = styled.div<{ height?: number }>`
  --background-color: #e7e9ef;
  --width: 100%;

  animation: shimmer 1.25s infinite;
  background-color: var(--background-color);
  background-repeat: no-repeat;
  background-image: linear-gradient(
    to right,
    var(--background-color) 0%,
    white 50%,
    var(--background-color) 100%
  );
  border-radius: 0.5rem;
  height: ${(p) => p.height ?? ROW_HEIGHT - 10}px;
  opacity: 0.5;
  width: var(--width);

  @keyframes shimmer {
    0% {
      background-position: calc(var(--width) * -1) 0;
    }

    50% {
      opacity: 0.25;
    }

    100% {
      background-position: var(--width) 0;
    }
  }
`;

// Table methods
export const resolveColumnWidth = (width: number, data: any[]) => {
  if (data.length) return width / Object.keys(data[0]).length;
  return 0;
};

export const resolveTableHeight = (dataLength: number) => {
  return HEADER_HEIGHT + ROW_HEIGHT * dataLength;
};

export const headerRenderer = ({ label }: TableHeaderProps) => {
  return <HeadItem>{label}</HeadItem>;
};

export const cellRenderer = ({ cellData }: TableCellProps) => {
  return <CellItem>{cellData}</CellItem>;
};

// table hooks
export const useGetTableInfo = ({
  data,
  isLoading,
  rows,
}: {
  data: any[];
  isLoading: boolean;
  rows: number;
}) => {
  const theme = useTheme();

  const resolveRowStyle = (isLastRow: boolean) => {
    if (isLastRow) return {};
    return {
      borderBottom: `1px solid ${theme.detailBlockBorder}`,
    };
  };

  const tableHeight = useMemo(() => {
    if (isLoading) return HEADER_HEIGHT + ROW_HEIGHT * rows;
    if (!data || !data.length) return 0;
    return resolveTableHeight(data.length);
  }, [data, isLoading, rows]);

  const noRowsRenderer = useCallback(() => {
    return (
      <NoRowsWrapper>
        {Array.from(Array(rows).keys()).map((r) => (
          <NoRowsRow key={r} />
        ))}
      </NoRowsWrapper>
    );
  }, [isLoading, rows]);

  return { resolveRowStyle, tableHeight, noRowsRenderer };
};
