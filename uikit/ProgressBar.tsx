import styled from "styled-components";

const Wrapper = styled.div`
  border-radius: 2px;
  background: ${(p) => p.theme.progressBarBackground};
  width: 100%;
  height: 4px;
`;

const Progress = styled.div<{ color: string; percentage: string }>`
  width: ${(p) => p.percentage};
  background: ${(p) => p.color};
  border-radius: 2px;
  height: 4px;
`;

interface IProgressBar {
  percentage: string;
  color: string;
}

export const ProgressBar = ({ percentage, color }: IProgressBar) => {
  return (
    <Wrapper>
      <Progress percentage={percentage} color={color} />
    </Wrapper>
  );
};
