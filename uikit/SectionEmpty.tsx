import styled from "styled-components";

const Container = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: center;
  padding: 20px 0 40px;
`;

const Text = styled.span``;

interface ISectionEmpty {
  text?: string;
}

export const SectionEmpty = ({ text }: ISectionEmpty) => {
  return (
    <Container>
      <Text>{text ?? "No data available"}</Text>
    </Container>
  );
};
