import Image from "next/image";

interface IAvatar {
  accountName: string;
  size: number;
}

interface AvatarColorSchemaOption {
  backgroundColor: string;
  contentColor: string;
}

const AvatarColorSchemas: AvatarColorSchemaOption[] = [
  {
    backgroundColor: "#ffdcb3", // orange
    contentColor: "#ff8c00",
  },
  {
    backgroundColor: "#bbb5fc", // purple
    contentColor: "#1a0aae",
  },
  {
    backgroundColor: "#c2f0e6", // green
    contentColor: "#28a489",
  },
  {
    backgroundColor: "#b6e4fb", // blue
    contentColor: "#0959a9",
  },
];

export const Avatar = ({ accountName, size }: IAvatar) => {
  const chooseColorSchema = (): AvatarColorSchemaOption => {
    let baseString = accountName;
    let charCodeSum = 0;
    for (const character of baseString.split("")) {
      charCodeSum += character.charCodeAt(0);
    }
    return AvatarColorSchemas[charCodeSum % 4];
  };

  const schema = chooseColorSchema();

  // TODO: Not a best way to check if valid url, just make sure CDN is always on
  // const checkLink = async (url: string) => (await fetch(url)).ok;

  const avatarUrl = `https://cdn.libre.org/avatars/${[
    schema.backgroundColor.substring(1),
  ]}/${[accountName[0].toLowerCase()]}.svg`;

  return (
    <div
      style={{
        width: size,
        height: size,
        borderRadius: size,
        backgroundColor: schema.backgroundColor,
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Image src={avatarUrl} height={size} width={size} />
    </div>
  );
};
