import SwitchSelector from "react-switch-selector";
import { SwitchSelectorProps } from "react-switch-selector/dist/SwitchSelector.props";
import styled from "styled-components";

const TOGGLE_BACKGROUND_COLOR = "#e2e2e9";
const TOGGLE_FONT_COLOR = "#686777";
const TOGGLE_BACKGROUND_COLOR_SELECTED = "#FFFFFF";
const TOGGLE_FONT_COLOR_SELECTED = "#110f28";

const ToggleButtonContainer = styled.div`
  margin-top: 30px;
  margin-bottom: 30px;
  width: 300px;
  height: 35px;
  align-self: center;
`;

export const ToggleSwitch = ({
  onChange,
  initialSelectedIndex,
  forcedSelectedIndex,
  labels,
}) => {
  const options: SwitchSelectorProps["options"] = [
    {
      label: labels[0],
      value: 0,
    },
    {
      label: labels[1],
      value: 1,
    },
  ];
  return (
    <ToggleButtonContainer>
      <SwitchSelector
        onChange={onChange}
        options={options}
        initialSelectedIndex={initialSelectedIndex}
        forcedSelectedIndex={forcedSelectedIndex}
        selectedFontColor={TOGGLE_FONT_COLOR_SELECTED}
        selectedBackgroundColor={TOGGLE_BACKGROUND_COLOR_SELECTED}
        backgroundColor={TOGGLE_BACKGROUND_COLOR}
        fontColor={TOGGLE_FONT_COLOR}
      />
    </ToggleButtonContainer>
  );
};
