export const EMPTY_BALANCE = "0 LIBRE";
export const TOKEN_PRECISION = 0;
export const SHORTENED_TOKEN_PRECISION = 0;
export const PROPOSALS_CONTRACT = "dao.libre"; // all proposal actions live on this contract
export const PROPOSALS_TOKEN = "LIBRE";
export const PROPOSALS_TRANSFER_TOKEN = "eosio.token"; // token contract that the token lives on (transfer and pulling balance data must use this)
export const PROPOSALS_FEE = "2000.0000 LIBRE";
export const MAX_ASSET_FEE_MULTIPLIER: number = 1.00012;
export const MIN_FEE_MULTIPLIER: number = 2 / 10;
export const EXPECTED_FEE_DIVIDER: number = 10000;
export const MARKUS_OFFERS_REFETCH_ATTEMPTS = 10;

export const MediaQueryWidths = {
  tiny: 400,
  small: 600,
  medium: 970,
  large: 1224,
};

export enum SendWalletValueTypes {
  USD = "USD",
  TOKEN = "TOKEN",
}

interface IChainConfig {
  [key: string]: any;
}

export const CHAIN_CONFIG = (): IChainConfig => {
  const environment = process.env.NEXT_PUBLIC_ENV;
  const chainId = process.env.NEXT_PUBLIC_CHAIN_ID;
  const coreAPIUrl = process.env.NEXT_PUBLIC_CORE_API_URL;
  const libreAPIUrl = process.env.NEXT_PUBLIC_LIBRE_API_URL;
  const libreAPIUrlBackup = process.env.NEXT_PUBLIC_LIBRE_API_BAK_URL;
  const hyperionAPIUrl = process.env.NEXT_PUBLIC_HYPERION_API_URL;
  const hyperionAPIUrlBackup = process.env.NEXT_PUBLIC_HYPERION_API_BAK_URL;
  if (environment && environment.toLowerCase() === "testnet") {
    return {
      coreAPI: {
        url: coreAPIUrl || "https://server.staging.bitcoinlibre.io",
      },
      hyperionAPI: {
        url: hyperionAPIUrl || "https://testnet.libre.org",
        swapContract: "evotest",
        backup_url: hyperionAPIUrlBackup ?? "https://testnet.libre.org",
        chainId:
          chainId ??
          "b64646740308df2ee06c6b72f34c0f7fa066d940e831f752db2006fcc2b78dee",
      },
      libreAPI: {
        url: libreAPIUrl || "https://testnet.libre.org",
        backup_url: libreAPIUrlBackup ?? "https://testnet.libre.org",
      },
      btc: {
        symbol: "BTCL",
        contract: "eosio.token",
        precision: 8,
        icon: "https://cdn.libre.org/icon-btc.svg",
      },
      usdt: {
        symbol: "USDL",
        contract: "eosio.token",
        precision: 6,
        icon: "https://cdn.libre.org/icon-usdt-v2.svg",
      },
      libre: {
        symbol: "LIBRE",
        contract: "eosio.token",
        precision: 4,
        icon: "https://cdn.libre.org/icon-libre-v2.svg",
      },
    };
  }
  return {
    coreAPI: {
      url: coreAPIUrl || "https://server.production.bitcoinlibre.io",
    },
    hyperionAPI: {
      url: hyperionAPIUrl || "https://lb.libre.org",
      swapContract: "swap.libre",
      backup_url: hyperionAPIUrlBackup ?? "https://lb.libre.org",
      chainId:
        chainId ??
        "38b1d7815474d0c60683ecbea321d723e83f5da6ae5f1c1f9fecc69d9ba96465",
    },
    libreAPI: {
      url: libreAPIUrl || "https://lb.libre.org",
      backup_url: libreAPIUrlBackup ?? "https://lb.libre.org",
    },
    btc: {
      symbol: "PBTC",
      contract: "btc.ptokens",
      precision: 9,
      icon: "https://cdn.libre.org/icon-btc.svg",
    },
    usdt: {
      symbol: "PUSDT",
      contract: "usdt.ptokens",
      precision: 9,
      icon: "https://cdn.libre.org/icon-usdt-v2.svg",
    },
    libre: {
      symbol: "LIBRE",
      contract: "eosio.token",
      precision: 4,
      icon: "https://cdn.libre.org/icon-libre-v2.svg",
    },
  };
};
