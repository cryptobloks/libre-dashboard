import { useMutation, useQueryClient } from "react-query";
import LibreClient from "../../../services/LibreClient";
import { RQ_FUNDING_STATS, RQ_USER_FUNDING_STATS } from "./keys";

export const useClaimFundingStake = () => {
  const queryClient = useQueryClient();
  return useMutation((id: number) => LibreClient.claimFunding(id), {
    onSuccess: async () => {
      queryClient.invalidateQueries(RQ_FUNDING_STATS);
      return queryClient.invalidateQueries([RQ_USER_FUNDING_STATS]);
    },
  });
};
