import { useMutation, useQueryClient } from "react-query";
import LibreClient from "../../../services/LibreClient";
import { RQ_USER_STAKES, RQ_USER_STAKE_STATS } from "./keys";

export const useClaimLibre = () => {
  const queryClient = useQueryClient();
  return useMutation((index: number) => LibreClient.claimLibre({ index }), {
    onSuccess: async () => {
      queryClient.invalidateQueries(RQ_USER_STAKE_STATS);
      return queryClient.invalidateQueries([RQ_USER_STAKES]);
    },
  });
};
