export const RQ_APPLICATION_STATS = "applicationStats";
export const RQ_STAKING_STATS = "stakeStats";
export const RQ_AIRDROP_STATS = "airdropStats";
export const RQ_MINT_STATS = "mintStats";
