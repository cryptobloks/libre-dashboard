import { useMutation, useQueryClient } from "react-query";
import LibreClient, { IMintLibre } from "../../../services/LibreClient";
import { RQ_USER_MINTS, RQ_USER_MINT_STATS } from "./keys";

export const useMintLibre = () => {
  const queryClient = useQueryClient();
  return useMutation(
    ({ quantity, duration, symbol }: IMintLibre) =>
      LibreClient.mintLibre({ quantity, duration, symbol }),
    {
      onSuccess: async (response) => {
        setTimeout(() => {
          queryClient.invalidateQueries(RQ_USER_MINTS);
          queryClient.invalidateQueries(RQ_USER_MINT_STATS);
        }, 3000);
        return response;
      },
    }
  );
};
