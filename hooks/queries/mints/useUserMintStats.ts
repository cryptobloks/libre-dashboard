import { useQuery, UseQueryResult } from "react-query";
import { IMintStats } from "../../../models/Mints";
import APIClient from "../../../services/APIClient";
import { RQ_USER_MINT_STATS } from "./keys";

const getUserMintStats = async (accountName: string) => {
  if (!accountName) return {};
  const data = await APIClient.fetchUserMintStats(accountName);
  return data;
};

export default function useUserMintStats(
  accountName: string
): UseQueryResult<IMintStats, Error> {
  return useQuery<IMintStats, Error>([RQ_USER_MINT_STATS, accountName], () =>
    getUserMintStats(accountName)
  );
}
