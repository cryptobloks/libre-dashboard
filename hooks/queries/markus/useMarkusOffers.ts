import React from "react";
import { useQuery, UseQueryResult } from "react-query";
import { IMarkusOffer } from "../../../models/Markus";
import MarkusClient from "../../../services/MarkusClient";
import { MARKUS_OFFERS_REFETCH_ATTEMPTS } from "../../../utils/constants";
import { RQ_MARKUS_OFFERS } from "./keys";

const getMarkusOffers = async (
  identifier: string,
  callCountRef: React.Ref<number>
) => {
  if (!identifier) return [];
  callCountRef!.current += 1;
  const data: any = await MarkusClient.fetchMarkusOffers(identifier);
  return data;
};

export default function useMarkusOffers(
  identifier: string,
  callCountRef: React.Ref<number>
): UseQueryResult<IMarkusOffer[], Error> {
  return useQuery<IMarkusOffer[], Error>(
    RQ_MARKUS_OFFERS,
    () => getMarkusOffers(identifier, callCountRef),
    {
      enabled:
        !!identifier && callCountRef!.current <= MARKUS_OFFERS_REFETCH_ATTEMPTS,
      refetchInterval: (data) => {
        if (data && data.length) return 5000;
        return 1000;
      },
    }
  );
}
