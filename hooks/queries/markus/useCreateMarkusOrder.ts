import { useMutation, useQueryClient } from "react-query";
import MarkusClient, {
  ICreateMarkusOrderPayload,
} from "../../../services/MarkusClient";
import { RQ_MARKUS_ORDER } from "./keys";

export const useCreateMarkusOrder = () => {
  const queryClient = useQueryClient();
  return useMutation(
    (payload: ICreateMarkusOrderPayload) =>
      MarkusClient.createMarkusOrder(payload),
    {
      onSuccess: async (data) => {
        queryClient.setQueryData(RQ_MARKUS_ORDER, data);
        return data;
      },
    }
  );
};
