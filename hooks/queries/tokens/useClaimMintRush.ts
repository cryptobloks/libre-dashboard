import { useContext } from "react";
import { useMutation, useQueryClient } from "react-query";
import { NotificationContext } from "../../../providers/NotificationProvider";
import LibreClient from "../../../services/LibreClient";
import { RQ_USER_FARMING } from "../farming/keys";
import { RQ_USER_MINT_RUSH, RQ_USER_POOL_SUPPLY } from "./keys";

export const useClaimMintRush = () => {
  const queryClient = useQueryClient();
  const { handleOnOpen: handleNotification } = useContext(NotificationContext);
  return useMutation(() => LibreClient.mintRushClaim(), {
    onSuccess: async (data) => {
      if (data && data.success && data.transactionId)
        handleNotification(data.transactionId);
      queryClient.invalidateQueries(RQ_USER_MINT_RUSH);
      queryClient.invalidateQueries(RQ_USER_POOL_SUPPLY);
      queryClient.invalidateQueries(RQ_USER_FARMING);
    },
  });
};
