import { useContext } from "react";
import { useMutation, useQueryClient } from "react-query";
import { ITransferToAddress } from "../../../models/Tokens";
import { NotificationContext } from "../../../providers/NotificationProvider";
import LibreClient from "../../../services/LibreClient";
import { RQ_TOKEN_INFORMATION, RQ_USER_TOKENS } from "./keys";

export const useTransferToAddress = () => {
  const queryClient = useQueryClient();
  const { handleOnOpen: handleNotification } = useContext(NotificationContext);
  return useMutation(
    (payload: ITransferToAddress) => LibreClient.transferToAddress(payload),
    {
      onSuccess: async (data) => {
        // DELAY ADDED TO ALLOW API TO UPDATE
        setTimeout(() => {
          if (data && data.success && data.transactionId)
            handleNotification(data.transactionId);
          queryClient.invalidateQueries([RQ_TOKEN_INFORMATION]);
          queryClient.invalidateQueries([RQ_USER_TOKENS]);
        }, 1000);
      },
    }
  );
};
