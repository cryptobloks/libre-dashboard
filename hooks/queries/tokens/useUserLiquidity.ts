import { useQuery, UseQueryResult } from "react-query";
import { IUserPoolSupply } from "../../../models/Tokens";
import LibreClient from "../../../services/LibreClient";
import { RQ_USER_POOL_SUPPLY } from "./keys";

const getUserLiquidity = async (accountName: string) => {
  const data: any = await LibreClient.getUserLiquidity(accountName);
  return data;
};

export default function useUserLiquidity(
  accountName: string
): UseQueryResult<IUserPoolSupply, Error> {
  return useQuery<IUserPoolSupply, Error>(
    [RQ_USER_POOL_SUPPLY, accountName],
    () => getUserLiquidity(accountName)
  );
}
