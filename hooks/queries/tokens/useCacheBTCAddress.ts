import { useMutation } from "react-query";
import ChainClient from "../../../services/ChainClient";

export const useCacheBTCAddress = () => {
  return useMutation((payload: any) => ChainClient.cacheBTCAddress(payload));
};
