import { useQuery, UseQueryResult } from "react-query";
import { IToken } from "../../../models/Tokens";
import APIClient from "../../../services/APIClient";
import { RQ_TOKENS } from "./keys";

const getTokens = async () => {
  const data: IToken[] = await APIClient.fetchTokens();
  return data;
};

export default function useTokens(): UseQueryResult<IToken[], Error> {
  return useQuery<IToken[], Error>([RQ_TOKENS], getTokens);
}
