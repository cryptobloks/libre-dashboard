import { useQuery, UseQueryResult } from "react-query";
import { IProposalProperties } from "../../../models/Proposals";
import LibreClient from "../../../services/LibreClient";
import { RQ_PROPOSAL_PROPERTIES } from "./keys";

const getDAOInformation = async () => {
  const data: IProposalProperties = await LibreClient.getDAOInformation();
  return data;
};

export default function useProposalProperties(): UseQueryResult<
  IProposalProperties,
  Error
> {
  return useQuery<IProposalProperties, Error>(
    [RQ_PROPOSAL_PROPERTIES],
    getDAOInformation
  );
}
