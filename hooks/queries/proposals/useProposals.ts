import { useQuery, UseQueryResult } from "react-query";
import { IProposal } from "../../../models/Proposals";
import LibreClient from "../../../services/LibreClient";
import { RQ_PROPOSALS } from "./keys";

const getProposals = async () => {
  const data: IProposal[] = await LibreClient.getProposals();
  return data;
};

export default function useProposals(): UseQueryResult<IProposal[], Error> {
  return useQuery<IProposal[], Error>([RQ_PROPOSALS], getProposals);
}
