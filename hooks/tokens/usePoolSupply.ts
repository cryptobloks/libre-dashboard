import useTableRows from "../queries/contract/useTableRows";

export const usePoolSupply = () => {
  const { data: BTCUSD } = useTableRows({
    code: "swap.libre",
    scope: "BTCUSD",
    table: "stat",
  });
  const { data: BTCLIB } = useTableRows({
    code: "swap.libre",
    scope: "BTCLIB",
    table: "stat",
  });

  if (!BTCUSD || !BTCLIB) return [];

  return [...BTCUSD, ...BTCLIB];
};
