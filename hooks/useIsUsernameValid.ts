import { debounce } from "lodash";
import { useCallback, useState } from "react";
import LibreClient from "../services/LibreClient";

export const useIsUsernameValid = () => {
  const [isValid, setIsValid] = useState<boolean>(false);

  const checkAvailableAccount = async (accountName: string) => {
    try {
      const result = await LibreClient.getUsername(accountName);
      if (result) return setIsValid(true);
      setIsValid(false);
    } catch (e) {
      setIsValid(false);
    }
  };

  const handleChange = useCallback(
    debounce((name: string) => {
      checkAvailableAccount(name);
    }, 500),
    []
  );

  return {
    handleChange,
    isValid,
  };
};
