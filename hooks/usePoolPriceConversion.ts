import { IPoolSupply, IUserToken } from "../models/Tokens";
import { MAX_ASSET_FEE_MULTIPLIER } from "../utils/constants";
interface IUsePoolPriceConversion {
  poolSupply: IPoolSupply[] | undefined;
  pool: string;
  userTokens: IUserToken[] | undefined;
  input1Token: IUserToken;
  input2Token: IUserToken;
  amount1: number;
  amount2: number;
}

interface IUsePoolPriceConversionResponse {
  convertedToken2Price: number;
  convertedToken1Price: number;
  toBuy: number;
  maxAsset1: number;
  maxAsset2: number;
  hasBalanceError: string | null;
}

const emptyResponse = {
  convertedToken2Price: 0.0,
  convertedToken1Price: 0.0,
  toBuy: 0,
  maxAsset1: 0,
  maxAsset2: 0,
  hasBalanceError: null,
};

export const usePoolPriceConversion = ({
  poolSupply,
  pool,
  userTokens,
  input1Token,
  input2Token,
  amount1,
  amount2,
}: IUsePoolPriceConversion): IUsePoolPriceConversionResponse => {
  if (!poolSupply || (!amount1 && !amount2) || !userTokens)
    return emptyResponse;

  let activePool;

  if (pool === "BTCUSD") {
    activePool = poolSupply[0];
  } else {
    activePool = poolSupply[1];
  }

  const pool1Asset = {
    price:
      Number(activePool.pool1.quantity.split(" ")[0]) /
      Number(activePool.supply.split(" ")[0]),
    symbol: activePool.pool1.quantity.split(" ")[1],
  };

  const pool2Asset = {
    price:
      Number(activePool.pool2.quantity.split(" ")[0]) /
      Number(activePool.supply.split(" ")[0]),
    symbol: activePool.pool2.quantity.split(" ")[1],
  };

  const poolAssets = [pool1Asset, pool2Asset];

  const input1TokenPrice = poolAssets.find(
    (a) => a.symbol === input1Token.symbol
  )?.price;
  const input2TokenPrice = poolAssets.find(
    (a) => a.symbol === input2Token.symbol
  )?.price;

  const convertedToken2Price =
    (amount1 / Number(input1TokenPrice)) * Number(input2TokenPrice);

  const convertedToken1Price =
    (amount2 / Number(input2TokenPrice)) * Number(input1TokenPrice);

  const input1TokenUserBalance = userTokens.find(
    (t) => t.symbol === input1Token.symbol
  )?.unstaked;
  const input2TokenUserBalance = userTokens.find(
    (t) => t.symbol === input2Token.symbol
  )?.unstaked;

  const toBuy =
    (amount1 / Number(input1TokenPrice)) * (1 - (MAX_ASSET_FEE_MULTIPLIER - 1));
  const maxAsset1 = amount1;
  const maxAsset2 = convertedToken2Price;

  const hasBalanceError = () => {
    if (!input1TokenUserBalance || !input2TokenUserBalance)
      return "Insufficient Balance";
    if (maxAsset1 > input1TokenUserBalance)
      return `Insufficient ${input1Token.symbol} Balance`;
    if (maxAsset2 > input2TokenUserBalance)
      return `Insufficient ${input2Token.symbol} Balance`;
    if (convertedToken2Price > input2TokenUserBalance)
      return `Insufficient ${input2Token.symbol} Balance`;
    return null;
  };

  return {
    convertedToken2Price,
    convertedToken1Price,
    toBuy,
    maxAsset1,
    maxAsset2,
    hasBalanceError: hasBalanceError(),
  };
};
