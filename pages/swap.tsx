import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import styled from "styled-components";
import LiquidityBox from "../components/swap/LiquidityBox";
import SwapBox from "../components/swap/SwapBox";
import { ToggleSwitch } from "../uikit/ToggleSwitch";
import { MediaQueryWidths } from "../utils/constants";

const Wrapper = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  background: ${(p) => p.theme.backgroundColor};
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: flex-start;
  align-items: center;

  @media (max-width: ${MediaQueryWidths.medium}px) {
    margin-top: 10px;
  }
`;

export default function Swap() {
  const router = useRouter();
  const [pageIndex, setPageIndex] = useState<number>(0);

  const handleSwitch = (newValue: number) => setPageIndex(newValue);

  const renderPage = () => {
    if (pageIndex === 0) return <SwapBox />;
    return <LiquidityBox />;
  };

  useEffect(() => {
    if (router.query && router.query.type && router.query.type == "liquidity") {
      setPageIndex(1);
    }
  }, [router.query]);

  return (
    <>
      <Head>
        <title>LIBRE | Swap</title>
      </Head>
      <Wrapper>
        <Container>
          <ToggleSwitch
            onChange={handleSwitch}
            initialSelectedIndex={pageIndex}
            forcedSelectedIndex={pageIndex}
            labels={["Swap", "Pool"]}
          />
          {renderPage()}
        </Container>
      </Wrapper>
    </>
  );
}
