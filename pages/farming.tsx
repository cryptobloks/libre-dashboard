import Head from "next/head";
import styled from "styled-components";
import { HeaderDetails } from "../components/HeaderDetails";
import { PageTitle } from "../components/PageTitle";
import { FarmingTable } from "../components/farming/FarmingTable";
import { SectionWrapper } from "../uikit/Sections";

const Wrapper = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  background: ${(p) => p.theme.backgroundColor};
`;

const Content = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`;

export default function Farming() {
  return (
    <>
      <Head>
        <title>LIBRE | Farming</title>
      </Head>
      <Wrapper>
        <Header>
          <PageTitle title="Farming" />
          <HeaderDetails />
        </Header>
        <Content>
          <SectionWrapper>
            <FarmingTable />
          </SectionWrapper>
        </Content>
      </Wrapper>
    </>
  );
}
