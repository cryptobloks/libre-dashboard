import { useRouter } from "next/router";
import { createContext, useContext, useEffect, useState } from "react";
import { useQueryClient } from "react-query";
import { RQ_USER_TOKENS } from "../hooks/queries/tokens/keys";
import { WalletType } from "../models/Client";
import LibreClient, { User } from "../services/LibreClient";

interface AuthContext {
  currentUser: User;
  isLoadingUser: boolean;
  login: ({
    type,
  }: {
    type: WalletType;
  }) => Promise<{ user: User; error: string }>;
  logout: () => Promise<void>;
}

interface Props {
  children: JSX.Element | JSX.Element[];
}

const fakeUser = {
  actor: "",
  permission: "",
};

export const AuthContext = createContext<AuthContext>({
  currentUser: fakeUser,
  isLoadingUser: true,
  login: () =>
    Promise.resolve({ user: { actor: "", permission: "" }, error: "" }),
  logout: () => Promise.resolve(),
});

export const useAuthContext = (): AuthContext => {
  const context = useContext(AuthContext);
  return context;
};

export const AuthProvider = ({ children }: Props): JSX.Element => {
  const queryClient = useQueryClient();
  const { query, pathname } = useRouter();

  const [isLoadingUser, setIsLoadingUser] = useState<boolean>(false);
  const [currentUser, setCurrentUser] = useState<User>(fakeUser);

  useEffect(() => {
    if (typeof window !== "undefined" && (!currentUser || !currentUser.actor)) {
      const restore = async () => {
        const { user, error } = await LibreClient.restoreSession();

        if (error || !user) {
          setIsLoadingUser(false);
          return;
        }

        setCurrentUser(user);
        setIsLoadingUser(false);
      };

      restore();
    }
  }, []);

  useEffect(() => {
    if (pathname !== "/[username]") return;
    if (!query.username) return;
    if (!currentUser.actor) {
      setCurrentUser({
        actor: query.username as string,
        permission: "inactive",
      });
    }
  }, [query, pathname]);

  const login = async ({
    type,
  }: {
    type: WalletType;
  }): Promise<{ user: User; error: any }> => {
    const { user, error } = await LibreClient.login({ type });
    setCurrentUser(user);
    return { user, error };
  };

  const logout = async () => {
    await LibreClient.logout();
    queryClient.removeQueries(RQ_USER_TOKENS);
    setCurrentUser({
      actor: "",
      permission: "",
    });
  };

  const value: AuthContext = {
    currentUser,
    isLoadingUser,
    login,
    logout,
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};
