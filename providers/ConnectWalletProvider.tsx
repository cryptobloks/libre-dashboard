import { createContext, FunctionComponent, useCallback, useState } from "react";
import Cookies from "js-cookie";
import { ConnectModal } from "../components/ConnectModal";
import { useAuthContext } from "./AuthProvider";
import { WalletType } from "../models/Client";
import { useRouter } from "next/router";

export const ConnectWalletContext = createContext<{
  isOpen: boolean;
  handleOnOpen: () => void;
  handleOnClose: () => void;
}>({
  isOpen: false,
  handleOnOpen: () => {},
  handleOnClose: () => {},
});

export const ConnectWalletProvider: FunctionComponent = ({ children }) => {
  const { login } = useAuthContext();
  const router = useRouter();

  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleOnOpen = useCallback(() => {
    const isFromLibre = Cookies.get("bitcoinlibre_is_opened_from_app");
    if (isFromLibre) {
      login({ type: WalletType.Libre });
      router.push("/wallet");
      return;
    }
    setIsOpen(true);
  }, []);

  const handleOnClose = useCallback(() => {
    setIsOpen(false);
  }, []);

  const value = {
    isOpen,
    handleOnClose,
    handleOnOpen,
  };

  return (
    <ConnectWalletContext.Provider value={value}>
      {children}
      <ConnectModal open={isOpen} handleOnClose={handleOnClose} />
    </ConnectWalletContext.Provider>
  );
};
