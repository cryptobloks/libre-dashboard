import axios from "axios";
import { pBTC } from "ptokens-pbtc";

class APIClient {
  libreAPIUrl: string | undefined;
  libreAPIPath: string | undefined;
  hyperionAPIUrl: string | undefined;

  constructor() {
    this.libreAPIUrl = process.env.NEXT_PUBLIC_LIBRE_API_URL;
    this.hyperionAPIUrl = process.env.NEXT_PUBLIC_HYPERION_API_URL;
  }

  // APPLICATION STATS ------------------- //
  fetchApplicationStats = async () => {
    try {
      const response = await axios.get(`${this.libreAPIUrl}/stats/chain`);
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/stats/chain`, e.message);
    }
  };

  fetchStakeStats = async () => {
    try {
      const response = await axios.get(`${this.libreAPIUrl}/stats/stake`);
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/stats/stake`, e.message);
    }
  };

  fetchAirdropStats = async () => {
    try {
      const response = await axios.get(`${this.libreAPIUrl}/stats/airdrop`);
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/stats/airdrop`, e.message);
    }
  };

  // REFERRALS ------------------ //
  fetchReferralStats = async (name: string) => {
    try {
      const response = await axios.get(
        `${this.libreAPIUrl}/referrals/stats/${name}`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/referrals/stats/${name}`, e.message);
    }
  };

  fetchMyReferrals = async (name: string) => {
    try {
      const response = await axios.get(
        `${this.libreAPIUrl}/referrals/user/${name}`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/referrals/user/${name}`, e.message);
    }
  };

  fetchReferralLeaderboard = async () => {
    try {
      const response = await axios.get(
        `${this.libreAPIUrl}/referrals/leaderboard`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/referrals/leaderboard`, e.message);
    }
  };

  // STAKES ----------------------- //
  fetchUserStakeStats = async (accountName: string) => {
    try {
      const response = await axios.get(
        `${this.libreAPIUrl}/stakes/${accountName}/stats`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/stakes/${accountName}/stats`, e.message);
    }
  };

  // MINTS ----------------------- //
  fetchMintStats = async () => {
    try {
      const response = await axios.get(`${this.libreAPIUrl}/mints/stats`);
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/stats/mint`, e.message);
    }
  };

  fetchUserMints = async (accountName: string) => {
    try {
      const response = await axios.get(
        `${this.libreAPIUrl}/mints/${accountName}/list`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/${accountName}/list`, e.message);
    }
  };

  fetchUserMintStats = async (accountName: string) => {
    try {
      const response = await axios.get(
        `${this.libreAPIUrl}/mints/${accountName}/stats`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/mints/${accountName}/stats`, e.message);
    }
  };

  // FUNDING ----------------------- //
  fetchFundingStats = async () => {
    try {
      const response = await axios.get(`${this.libreAPIUrl}/contributions`);
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/contributions`, e.message);
    }
  };

  fetchUserFundingStats = async (accountName: string) => {
    try {
      const response = await axios.get(
        `${this.libreAPIUrl}/contributions/${accountName}`
      );
      return response.data;
    } catch (e) {
      console.log(
        `${this.libreAPIUrl}/contributions/${accountName}`,
        e.message
      );
    }
  };

  // New method for fetching user's total claimed
  fetchUserTotalClaimed = async (accountName: string) => {
    try {
      const response = await axios.post(
        `${this.hyperionAPIUrl}/v1/chain/get_table_rows`,
        {
          code: "daofnd.libre",
          table: "distribution",
          scope: "LIBRE",
          key_type: "name",
          upper_bound: accountName,
          json: true,
        }
      );

      // Filter the rows by accountName
      const filteredRow = response.data.rows.find(
        (row: { account: string }) => row.account === accountName
      );

      // If no rows match the accountName, return null
      if (!filteredRow) {
        console.log("No matching account found in response");
        return null;
      }

      // Otherwise, return the total_claimed of the first matching row
      if (filteredRow.total_claimed) {
        return filteredRow.total_claimed;
      } else {
        console.log("total_claimed not found in response");
        return null;
      }
    } catch (e) {
      console.log(`Error fetching user's total claimed:`, e);
    }
  };
  // TOKENS ----------------------- //
  fetchTokens = async () => {
    try {
      const response = await axios.get(`${this.libreAPIUrl}/tokens`);
      return response.data;
    } catch (e: any) {
      console.log(`${this.libreAPIUrl}/tokens`, e.message);
    }
  };

  fetchWrappingAdresses = async (accountName: string, amount: number) => {
    try {
      const response = await axios.get(
        `${this.libreAPIUrl}/wrapping/${accountName}?amount=${amount}`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/tokens/${accountName}`, e.message);
    }
  };

  fetchLightningWrappingAdresses = async (
    accountName: string,
    amount: number
  ) => {
    try {
      const response = await axios.get(
        `${this.libreAPIUrl}/wrapping/lightning/${accountName}?amount=${amount}`
      );
      return response.data;
    } catch (e) {
      console.log(
        `${this.libreAPIUrl}/wrapping/lightning/${accountName}?amount=${amount}`,
        e.message
      );
    }
  };

  fetchBitcoinWrappingAdresses = async (accountName: string) => {
    try {
      const response = await axios.get(
        `${this.libreAPIUrl}/wrapping/bitcoin/${accountName}`
      );
      return response.data;
    } catch (e) {
      console.log(
        `${this.libreAPIUrl}/wrapping/bitcoin/${accountName}`,
        e.message
      );
    }
  };

  // VALIDATORS ----------------------- //
  fetchValidators = async () => {
    try {
      const response = await axios.get(`${this.libreAPIUrl}/producers`);
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/producers`, e.message);
    }
  };

  fetchValidatorVotes = async (accountName: string) => {
    try {
      const response = await axios.get(
        `${this.libreAPIUrl}/voted/${accountName}`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/voted/${accountName}`, e.message);
    }
  };

  // AUDIT --------------------------- //
  fetchAuditStats = async () => {
    try {
      const response = await axios.post(
        `${this.hyperionAPIUrl}/v1/chain/get_currency_stats`,
        {
          code: "btc.ptokens",
          symbol: "pbtc",
        }
      );
      return response.data;
    } catch (e) {
      console.log(
        `${this.hyperionAPIUrl}/v1/chain/get_currency_stats`,
        e.message
      );
    }
  };

  // Static URL neeeds to be refactored --------------------------- //
  fetchAuditList = async () => {
    try {
      const response = await axios.get(
        `https://audit.libreblocks.com/audit/index`
      );
      return response.data;
    } catch (e) {
      console.log(
        `https://audit.libreblocks.com/audit/index`,
        e.message
      );
    }
  };

  // FARMING --------------------------- //
  fetchFarmingStats = async () => {
    try {
      const response = await axios.get(`${this.libreAPIUrl}/farming/stats`);
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/farming/stats`, e.message);
      throw new Error(e.message);
    }
  };

  fetchUserFarming = async (accountName: string) => {
    try {
      const response = await axios.get(
        `${this.libreAPIUrl}/farming/${accountName}`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.libreAPIUrl}/farming/${accountName}`, e.message);
    }
  };

  // Static URL neeeds to be refactored --------------------------- //
  fetchUserBTCAddress = async (accountName: string) => {
    const _pbtc = new pBTC({
      nativeBlockchain: "bitcoin",
      nativeNetwork: "mainnet",
      hostBlockchain: "libre",
      hostNetwork: "mainnet",
      eosRpc: " http://lb.libre.org",
    });
    const depositAddressObject = await _pbtc.getDepositAddress(accountName);
    const depositAddress = depositAddressObject.value;
    return depositAddress;
  };
}

export default new APIClient();
