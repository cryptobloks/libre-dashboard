import styled, { useTheme } from "styled-components";
import { DaoFundProgressBar } from "./ProgressBar";
import { MediumButton } from "../../../uikit/Button";
import useFundingStats from "../../../hooks/queries/funding/useFundingStats";
import { satsToBitcoin } from "../../../utils";
import { useRouter } from "next/router";

const Wrapper = styled.div`
  padding: 24px;
  border-radius: 16px;
  border: 1px solid ${(p) => p.theme.detailBlockBorder};
  background-color: #fff;
`;

const Header = styled.div`
  display: flex;
  justify-content: center;
  margin: 10px 0 30px;
`;

const HeaderCol = styled.div`
  display: flex;
  flex-direction: column;
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: #110f28;
  display: inline-block;
  margin: auto;
`;

const ContributionText = styled.div`
  display: flex;
  justify-content: center;
  margin: 20px 0;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 20px 0;
`;

export const DaoFundSection = () => {
  const router = useRouter();

  const theme = useTheme();
  const { data: fundingStats } = useFundingStats();

  const handlePress = () => {
    router.push("/fund");
  };

  return (
    <Wrapper>
      <Header>
        <HeaderCol>
          <Title>LibreLaunch DAO Fund</Title>
        </HeaderCol>
      </Header>
      <DaoFundProgressBar
        // TODO: Hardcoded because end of funding  
        // day={fundingStats?.currentPeriod ? fundingStats?.currentPeriod + 1 : 0}
        day={60}
        btcContributed={
          fundingStats?.currentContributed
            ? satsToBitcoin(fundingStats?.currentContributed).toLocaleString()
            : "-"
        }
      />
      <ContributionText>
        Total Contributions:{" "}
        {fundingStats?.totalContributed
          ? satsToBitcoin(fundingStats?.totalContributed).toLocaleString()
          : "-"}{" "}
        BTC
      </ContributionText>
      <ButtonContainer>
        <MediumButton
          onClick={handlePress}
          text="Contribute Now"
          color={theme.buttonOrangeText}
          backgroundColor={theme.buttonOrange}
        />
      </ButtonContainer>
    </Wrapper>
  );
};
