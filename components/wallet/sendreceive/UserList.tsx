import Image from "next/image";
import { useCallback, useContext, useRef } from "react";
import styled from "styled-components";
import { useClickedOutside } from "../../../hooks/useClickedOutside";
import { ISearchUser } from "../../../models/Wallet";
import { WalletSendContext } from "../../../providers/WalletSendProvider";
import { Avatar } from "../../../uikit/Avatar";
import { UserListSkeleton } from "./UserListSkeleton";

const List = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  width: 100%;
  border: 1px solid ${(p) => p.theme.inputBorder};
  border-radius: 8px;
  margin-top: 5px;
  background: #ffffff;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.15);
`;

const ListItem = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  padding: 12px 14px;
  overflow: hidden;

  &:hover {
    opacity: 0.8;
  }
`;

const Left = styled.div`
  display: flex;
  align-items: center;

  > div {
    margin-right: 10px;
  }
`;

const Right = styled.div`
  display: flex;
  align-items: center;
  margin-left: auto;
`;

export const UserList = () => {
  const wrapperRef = useRef(null);
  const { searchUsers, isLoading, setRecipient, setSearchQuery } =
    useContext(WalletSendContext);

  const handleClickedOutside = useCallback(() => {
    setRecipient(null);
    setSearchQuery("");
  }, [setRecipient]);

  useClickedOutside(wrapperRef, handleClickedOutside);

  if (isLoading)
    return (
      <List ref={wrapperRef}>
        <ListItem>
          <UserListSkeleton />
        </ListItem>
      </List>
    );
  if (!searchUsers) return <></>;
  return (
    <List ref={wrapperRef}>
      {searchUsers.map((d: ISearchUser) => {
        return (
          <ListItem
            onClick={() =>
              setRecipient({
                destination: d.payer,
                type: "libre",
              })
            }
            key={d.payer}
          >
            <Left>
              <Avatar accountName={d.payer} size={25} />
              {d.payer}
            </Left>
            <Right>
              <Image
                src={"/icons/chevron-down.svg"}
                width={16}
                height={16}
                style={{ transform: "rotate(-90deg)" }}
              />
            </Right>
          </ListItem>
        );
      })}
    </List>
  );
};
