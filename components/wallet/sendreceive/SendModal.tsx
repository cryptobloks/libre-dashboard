import { useContext, useEffect } from "react";
import { WalletSendContext } from "../../../providers/WalletSendProvider";
import AppModal from "../../AppModal";
import { SendToSelect } from "./SendToSelect";

export const SendModal = ({
  open,
  handleClose,
}: {
  open: boolean;
  handleClose: () => void;
}) => {
  const { resetState } = useContext(WalletSendContext);

  useEffect(() => {
    if (open) return;
    resetState();
  }, [open]);

  return (
    <AppModal title="Send" show={open} onClose={handleClose}>
      <SendToSelect />
    </AppModal>
  );
};
