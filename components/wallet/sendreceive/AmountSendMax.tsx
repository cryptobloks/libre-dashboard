import { useContext } from "react";
import styled from "styled-components";
import useUserTokens from "../../../hooks/queries/tokens/useUserTokens";
import { useAuthContext } from "../../../providers/AuthProvider";
import { WalletSendContext } from "../../../providers/WalletSendProvider";
import { formatRoundedDownTokenPrecision } from "../../../utils";

const Container = styled.div`
  text-align: center;
  cursor: pointer;
`;

export const AmountSendMax = ({ handleChange, children }) => {
  const { currentUser } = useAuthContext();
  const { data: userTokens } = useUserTokens({
    accountName: currentUser.actor,
    notifyOnChangeProps: ["data"],
  });
  const { selectedToken, setAmount } = useContext(WalletSendContext);

  const handleClick = () => {
    const token = userTokens?.find((t) => t.symbol === selectedToken.symbol);
    if (!token) return;
    setAmount(
      formatRoundedDownTokenPrecision({
        value: Number(token?.unstaked),
        precision: token?.precision,
      })
    );
    handleChange(
      formatRoundedDownTokenPrecision({
        value: Number(token?.unstaked),
        precision: token?.precision,
      })
    );
  };

  return <Container onClick={handleClick}>{children}</Container>;
};
