import Image from "next/image";
import { useEffect, useRef, useState } from "react";
import styled, { useTheme } from "styled-components";
import { getLightningWrappingAdresses } from "../../../hooks/queries/tokens/useGetLightningWrappingAdress";
import { useAuthContext } from "../../../providers/AuthProvider";
import { MediumButton } from "../../../uikit/Button";

const AmountDescription = styled.div`
  color: ${(p) => p.theme.labelGrey};
  font-size: 14px;
  margin: 0 30px;
  overflow-wrap: break-word;
  text-align: center;
`;

const InputContainer = styled.div`
  text-align: center;
  margin: 100px 0;
  font-size: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Input = styled.input`
  resize: horizontal;
  width: 200px;
  border: none;
  background-color: #fafafa;
  font-size: 30px;
  font-family: inherit;
  padding: 0;
  margin: 0;

  :focus {
    border: none;
    outline: none;
  }
`;

const AmountContainer = styled.div`
  background-color: #fafafa;
  margin: -24px;
  border-top: 1px solid ${(p) => p.theme.labelGreenBackground};
  border-bottom-left-radius: 50px;
  border-bottom-right-radius: 50px;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 25px 0;
  margin-top: 50px;

  button {
    width: 90%;
  }
`;

const ErrorContainer = styled.div`
  background-color: #fbeaed;
  padding: 20px;
  width: 90%;
  margin: 30px auto;
  display: flex;
  border-radius: 10px;
`;

const ErrorIconContainer = styled.div`
  padding: 0 20px;
`;
const ErrorTextContainer = styled.div``;

const ErrorContainerTitle = styled.div`
  font-size: 15px;
  margin-bottom: 5px;
`;

const ErrorContainerDescription = styled.div`
  color: ${(p) => p.theme.labelGrey};
  font-size: 12px;
`;

export const LightinngAmountModal = ({
  amount,
  setAmount,
  setInvoice,
  invoice,
}: {
  amount: string;
  invoice: string | null;
  setAmount: React.Dispatch<React.SetStateAction<string>>;
  setInvoice: React.Dispatch<React.SetStateAction<string>>;
}) => {
  const theme = useTheme();
  const { currentUser } = useAuthContext();
  const span = useRef() as React.MutableRefObject<HTMLSpanElement>;
  const [width, setWidth] = useState(0);
  const [lightningInvoiceLoading, setLightningInvoiceLoading] = useState(false);

  useEffect(() => {
    if (span && span.current && span.current.offsetWidth) {
      setWidth(span.current.offsetWidth);
    }
  }, [amount]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const inputVal = e.currentTarget.value;
    setAmount(Number(inputVal).toString());
  };

  const handleConfirmAmount = async () => {
    setLightningInvoiceLoading(true);
    const { lightning } = await getLightningWrappingAdresses(
      currentUser.actor,
      Number(amount)
    );
    setInvoice(lightning);
    if (lightning === null) {
      setLightningInvoiceLoading(false);
    }
  };

  return (
    <AmountContainer>
      <InputContainer>
        <span style={{ visibility: "hidden", position: "absolute" }} ref={span}>
          {amount}
        </span>
        <Input
          min={0}
          type="number"
          id="number-input"
          step={1}
          pattern="\d+"
          autoFocus
          value={amount}
          onChange={handleChange}
          size={1}
          style={{ width: width + 10 }}
        />
        &nbsp;SATS
      </InputContainer>

      {invoice === null ? (
        <ErrorContainer>
          <ErrorIconContainer>
            <Image
              src="/icons/icons_error.svg"
              width={35}
              height={35}
              alt="icon-error"
            />
          </ErrorIconContainer>
          <ErrorTextContainer>
            <ErrorContainerTitle>Something went wrong</ErrorContainerTitle>
            <ErrorContainerDescription>
              Unfortunately,you cannot create more invoices, please try again in
              24 hours
            </ErrorContainerDescription>
          </ErrorTextContainer>
        </ErrorContainer>
      ) : (
        <></>
      )}

      <AmountDescription>
        To create a lightning invoice you need to define the amount of Bitcoin
        you want to receive.
      </AmountDescription>
      <ButtonContainer>
        <MediumButton
          text="Confirm Amount"
          onClick={handleConfirmAmount}
          color={invoice === null ? "#b4b3bc" : theme.buttonOrangeText}
          backgroundColor={invoice === null ? "#f5f5f6" : theme.buttonOrange}
          isLoading={lightningInvoiceLoading}
          disabled={invoice === null}
        />
      </ButtonContainer>
    </AmountContainer>
  );
};
