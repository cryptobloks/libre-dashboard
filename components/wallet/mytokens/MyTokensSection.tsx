import styled from "styled-components";
import { useTokenTableData } from "../../../hooks/useTokenTableData";
import { useUSDConversion } from "../../../hooks/useUSDConversion";
import { WalletSendProvider } from "../../../providers/WalletSendProvider";
import { SectionEmpty } from "../../../uikit/SectionEmpty";
import { SectionWrapper } from "../../../uikit/Sections";
import { formatCurrencyWithPrecision } from "../../../utils";
import { MyTokensTable } from "./MyTokensTable";
import { MyTokensTableStatic } from "./MyTokensTableStatic";

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 10px 0 30px;
`;

const HeaderCol = styled.div`
  display: flex;
  flex-direction: column;
`;

const HeaderRight = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 14px;
  font-weight: 600;
  line-height: 1.25;
  color: #110f28;
  height: 24px;
  justify-content: center;
`;

const HeaderDescription = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  margin-top: 10px;
  color: ${(p) => p.theme.descriptionText};
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: #110f28;
  display: inline-block;
`;

export const MyTokensSection = ({ isStatic }: { isStatic?: boolean }) => {
  const { data, isLoading } = useTokenTableData();
  const total = useUSDConversion(data);

  const renderContent = () => {
    if ((!data || !data.length) && !isLoading) return <SectionEmpty />;
    if (isStatic)
      return <MyTokensTableStatic data={data} isLoading={isLoading} />;
    return <MyTokensTable data={data} isLoading={isLoading} />;
  };

  return (
    <>
      <SectionWrapper>
        <Header>
          <HeaderCol>
            <Title>My Tokens</Title>
            <HeaderDescription>
              USDT and BTC are trustlessly wrapped using P.Network.{" "}
              <a
                href="https://libre-chain.gitbook.io/libre-docs/cross-chain-interoperability/pnetwork"
                target="_blank"
                style={{ color: "#ff8c00", textDecoration: "underline" }}
                rel="noreferrer"
              >
                Learn more.
              </a>
            </HeaderDescription>
          </HeaderCol>
          <HeaderRight>
            Total Value: ${formatCurrencyWithPrecision(total, 2)}
          </HeaderRight>
        </Header>
        <WalletSendProvider>{renderContent()}</WalletSendProvider>
      </SectionWrapper>
    </>
  );
};
