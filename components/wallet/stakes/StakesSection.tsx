import { orderBy } from "lodash";
import moment from "moment";
import Image from "next/image";
import { useMemo, useState } from "react";
import styled, { useTheme } from "styled-components";
import useUserStakes from "../../../hooks/queries/stakes/useUserStakes";
import { IStake } from "../../../models/Stakes";
import { useAuthContext } from "../../../providers/AuthProvider";
import { MediumButton } from "../../../uikit/Button";
import { SectionEmpty } from "../../../uikit/SectionEmpty";
import { SectionWrapper } from "../../../uikit/Sections";
import { MediaQueryWidths } from "../../../utils/constants";
import { StakeModal } from "../../stake/StakeModal";
import StakeTable from "./StakeTable";

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 10px 0 30px;
`;

const HeaderCol = styled.div`
  display: flex;
  flex-direction: column;
`;

const HeaderDescription = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  margin-top: 10px;
  color: ${(p) => p.theme.descriptionText};
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: #110f28;
  display: inline-block;
`;

const Btn = styled.div`
  display: flex;
  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: none;
  }
`;

const MobileButton = styled.div`
  display: none;

  button {
    min-width: 42px;
  }
  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: flex;
  }
`;

export const StakesSection = () => {
  const { currentUser } = useAuthContext();
  const { data, isLoading } = useUserStakes(currentUser.actor);
  const theme = useTheme();

  const [modalOpen, setModalOpen] = useState<boolean>(false);

  // TEMPORARY FIX TO HIDE CANCELLED STAKES
  const items = useMemo(() => {
    if (!data) return [];
    const filteredData = data.filter((s: IStake) => s.status !== 3);
    return orderBy(
      filteredData,
      (o: any) => {
        return moment(o.payout_date);
      },
      ["desc"]
    );
  }, [data]);

  const renderContent = () => {
    if ((!items || !items.length) && !isLoading) return <SectionEmpty />;
    return <StakeTable data={items} isLoading={isLoading} />;
  };

  return (
    <>
      <SectionWrapper>
        <Header>
          <HeaderCol>
            <Title>My Stakes</Title>
            <HeaderDescription>
              Stake longer and earlier for higher yields.{" "}
              <a
                href="https://libre-chain.gitbook.io/libre-docs/earn/staking"
                target="_blank"
                style={{ color: "#ff8c00", textDecoration: "underline" }}
                rel="noreferrer"
              >
                Learn more.
              </a>
            </HeaderDescription>
          </HeaderCol>
          <HeaderCol>
            <Btn>
              <MediumButton
                color={theme.buttonOrangeText}
                backgroundColor={theme.buttonOrange}
                text="New Stake"
                onClick={() => setModalOpen(!modalOpen)}
              />
            </Btn>
            <MobileButton>
              <MediumButton
                color={theme.buttonOrangeText}
                backgroundColor={theme.buttonOrange}
                text={
                  <Image src={"/icons/plus-icon.svg"} width={12} height={12} />
                }
                onClick={() => setModalOpen(!modalOpen)}
              />
            </MobileButton>
          </HeaderCol>
        </Header>
        {renderContent()}
        <StakeModal
          open={modalOpen}
          handleOnClose={() => setModalOpen(false)}
        />
      </SectionWrapper>
    </>
  );
};
