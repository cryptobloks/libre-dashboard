import {
  AutoSizer,
  Column,
  InfiniteLoader,
  Table,
  TableCellProps,
} from "react-virtualized";
import styled from "styled-components";
import { IMyReferral } from "../../../models/Referrals";
import { displayTokenPrecisions } from "../../../models/Tokens";
import {
  headerRenderer,
  TableWrapper,
  useGetTableInfo,
} from "../../../uikit/Table";
import { formatCurrencyWithPrecision } from "../../../utils";
import { TokenWithUSDValue } from "../../TokenWithUSDValue";

const CellItem = styled.span`
  font-size: 15px;
  color: black;
`;

interface IMyReferralsTable {
  data: IMyReferral[];
  isLoading: boolean;
}

const MyReferralsTable = ({ data, isLoading }: IMyReferralsTable) => {
  const { resolveRowStyle, tableHeight, noRowsRenderer } = useGetTableInfo({
    data,
    isLoading,
    rows: 1,
  });

  const isRowLoaded = ({ index }: { index: number }) => !!data[index];

  const loadMoreRows = async () => {
    return;
  };

  const cellRenderer = ({ cellData, dataKey }: TableCellProps) => {
    if (dataKey === "ranking") return <CellItem>{`#${cellData}`}</CellItem>;
    if (dataKey === "earnings")
      return (
        <CellItem>
          <TokenWithUSDValue
            tokenSymbol={"LIBRE"}
            value={formatCurrencyWithPrecision(
              Number(cellData),
              displayTokenPrecisions["LIBRE"]
            )}
          />
        </CellItem>
      );
    return <CellItem>{cellData}</CellItem>;
  };

  return (
    <TableWrapper height={tableHeight}>
      <AutoSizer>
        {({ width, height }) => (
          <InfiniteLoader
            isRowLoaded={isRowLoaded}
            loadMoreRows={loadMoreRows}
            rowCount={1}
          >
            {({ onRowsRendered, registerChild }) => (
              <Table
                ref={registerChild}
                width={width}
                height={height}
                headerHeight={30}
                rowHeight={80}
                rowCount={data.length}
                rowGetter={({ index }) => data[index]}
                rowStyle={({ index }) =>
                  resolveRowStyle(index + 1 === data.length)
                }
                onRowsRendered={onRowsRendered}
                noRowsRenderer={noRowsRenderer}
                overscanRowCount={5}
              >
                <Column
                  label="Rank"
                  dataKey={"ranking"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Libre name"
                  dataKey={"accountName"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Earned"
                  dataKey={"earnings"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="People Joined"
                  dataKey={"referred"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
              </Table>
            )}
          </InfiniteLoader>
        )}
      </AutoSizer>
    </TableWrapper>
  );
};

export default MyReferralsTable;
