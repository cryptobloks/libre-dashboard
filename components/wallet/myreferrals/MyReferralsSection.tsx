import { useState } from "react";
import styled, { useTheme } from "styled-components";
import useMyReferrals from "../../../hooks/queries/referrals/useMyReferrals";
import { useAuthContext } from "../../../providers/AuthProvider";
import { MediumButton } from "../../../uikit/Button";
import MyReferralsTable from "./MyReferralTable";
import { ReferralInviteModal } from "./ReferralInviteModal";
import { ReferralsEmptyState } from "./ReferralsEmptyState";

const Wrapper = styled.div`
  padding: 24px 24px 0;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
  background-color: #fff;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 10px 0 30px;
`;

const HeaderCol = styled.div`
  display: flex;
  flex-direction: column;
`;

const HeaderDescription = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  margin-top: 10px;
  color: ${(p) => p.theme.descriptionText};
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: #110f28;
  display: inline-block;
`;

export const MyReferralsSection = () => {
  const { currentUser } = useAuthContext();
  const { data = [], isLoading } = useMyReferrals(currentUser.actor);
  const theme = useTheme();

  const [modalOpen, setModalOpen] = useState<boolean>(false);

  if ((!data || !data.length) && !isLoading) return <ReferralsEmptyState />;

  return (
    <>
      <Wrapper>
        <Header>
          <HeaderCol>
            <Title>My Referrals</Title>
            <HeaderDescription>
              Earn LIBRE when others you invite stake, spin and win. Learn more.
            </HeaderDescription>
          </HeaderCol>
          <HeaderCol>
            <MediumButton
              color={theme.buttonOrangeText}
              backgroundColor={theme.buttonOrange}
              text="Invite"
              onClick={() => setModalOpen(!modalOpen)}
            />
          </HeaderCol>
        </Header>
        <MyReferralsTable data={data} isLoading={isLoading} />
        <ReferralInviteModal
          open={modalOpen}
          handleOnClose={() => setModalOpen(false)}
        />
      </Wrapper>
    </>
  );
};
