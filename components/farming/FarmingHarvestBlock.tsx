import moment from "moment";
import "moment-timezone";
import Image from "next/image";
import { useEffect, useMemo, useRef } from "react";
import { useCountUp } from "react-countup";
import styled, { useTheme } from "styled-components";
import { useFarmingClaim } from "../../hooks/queries/farming/useFarmingClaim";
import useFarmingStats from "../../hooks/queries/farming/useFarmingStats";
import useUserFarming from "../../hooks/queries/farming/useUserFarming";
import { useAuthContext } from "../../providers/AuthProvider";
import { MediumButton } from "../../uikit/Button";

const ActionCol = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;

  padding: 20px;
  border-radius: 8px;
  border: 1px solid ${(p) => p.theme.detailBlockBorder};
`;

const ActionTitle = styled.span`
  text-transform: uppercase;
  font-size: 14px;
  font-weight: 600;
  line-height: 1.71;
  letter-spacing: 2px;
  color: ${(p) => p.theme.descriptionText};
  display: inline-block;
`;

const ActionDescription = styled.span`
  margin-bottom: 20px;
  font-size: 12px;
  color: ${(p) => p.theme.descriptionText};
`;

const ActionBtm = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin-top: auto;
`;

const ActionBtmLeft = styled.div`
  display: flex;
  align-items: center;
`;

const ActionIcons = styled.div`
  display: flex;
  flex-shrink: 0;
  margin-right: 20px;
`;

const ActionText = styled.span`
  font-size: 33.2px;
  font-weight: 600;
  line-height: 1.21;
  color: ${(p) => p.theme.titleText};
`;

const ActionBtn = styled.div``;

export const FarmingHarvestBlock = ({ pool }: { pool: string }) => {
  const counterRef = useRef(null);

  const theme = useTheme();
  const { currentUser } = useAuthContext();
  const { data: userFarming } = useUserFarming(currentUser.actor);
  const { data: farmingStats } = useFarmingStats();
  const { mutateAsync: farmingClaim } = useFarmingClaim();

  const data = useMemo(() => {
    let animationDecimals = 4;
    const userData = userFarming?.find((item) => item.symbol === pool);
    const farmingData = farmingStats?.find((item) => item.symbol === pool);
    const myFarmPercentage = Number(
      Number(userData?.total_staked) / Number(farmingData?.farming_staked)
    );

    if (myFarmPercentage > 0.01) animationDecimals = 2;
    if (myFarmPercentage > 0.001 && myFarmPercentage < 0.01)
      animationDecimals = 3;
    if (myFarmPercentage < 0.001) animationDecimals = 4;

    return {
      rewardPerFarm:
        Number(farmingData?.reward_per_farm) &&
        Number(farmingData?.reward_per_farm) > 0
          ? Number(farmingData?.reward_per_farm)
          : 0,
      userUnclaimed: Number(userData?.unclaimed),
      myFarmPercentage,
      animationDecimals,
    };
  }, [userFarming, farmingStats]);

  const { update } = useCountUp({
    ref: counterRef,
    start: 0,
    end: 0,
    duration: 0.5,
    decimals: data.animationDecimals,
  });

  useEffect(() => {
    const interval = setInterval(() => {
      const timeNow = moment().unix();
      const lastUpdate = moment
        .tz(moment(), "America/Los_Angeles")
        .startOf("day")
        .unix();
      const secondsPassed = timeNow - lastUpdate;
      const blocksPassed = secondsPassed * 2;
      const rewardsEarned =
        (data.userUnclaimed +
          blocksPassed * data.myFarmPercentage * data.rewardPerFarm) /
        2;
      if (rewardsEarned) update(Number(rewardsEarned));
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  }, [data]);

  return (
    <ActionCol>
      <ActionTitle>Libre Earned (Approx)</ActionTitle>
      <ActionDescription>
        {data.userUnclaimed === 0 &&
          `Next harvest available: ${moment()
            .local()
            .add(1, "day")
            .startOf("day")
            .format("MMM Do, YYYY")}`}
      </ActionDescription>
      <ActionBtm>
        <ActionBtmLeft>
          <ActionIcons>
            <Image src="/icons/libre-asset-icon.svg" width={30} height={30} />
          </ActionIcons>
          <ActionText ref={counterRef} />
        </ActionBtmLeft>
        <ActionBtn>
          {data.userUnclaimed > 0 ? (
            <MediumButton
              disabled={data.userUnclaimed === 0}
              text={"Harvest"}
              onClick={() => farmingClaim(pool)}
              color={theme.buttonOrangeText}
              backgroundColor={theme.buttonOrange}
            />
          ) : (
            <MediumButton
              disabled
              text={`Harvest`}
              color={theme.buttonGreyText}
              backgroundColor={"#f5f5f6"}
              border={theme.inputBorder}
              onClick={() => {}}
            />
          )}
        </ActionBtn>
      </ActionBtm>
    </ActionCol>
  );
};
