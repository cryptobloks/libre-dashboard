import { useEffect, useMemo, useState } from "react";
import styled, { useTheme } from "styled-components";
import { useFarmingStake } from "../../hooks/queries/farming/useFarmingStake";
import { useFarmingWithdraw } from "../../hooks/queries/farming/useFarmingWithdraw";
import useUserLiquidity from "../../hooks/queries/tokens/useUserLiquidity";
import { IUserFarming } from "../../models/Farming";
import {
  IUserPoolSupply,
  liquidityTokenPairs,
  liquidityTokenPrecisions,
} from "../../models/Tokens";
import { useAuthContext } from "../../providers/AuthProvider";
import { MediumButton } from "../../uikit/Button";
import { InputSlider } from "../../uikit/InputSlider";
import { Notification } from "../../uikit/Notification";
import { formatRoundedDownTokenPrecision } from "../../utils";
import AppModal from "../AppModal";

const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const SliderContainer = styled.div`
  &:first-of-type {
    margin: 35px 0;
  }

  &:last-of-type {
    margin-bottom: 20px;
  }
`;

const BottomContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: -24px;
  margin-top: 24px;
  padding: 24px;
  background-color: #fafafa;
  border-top: 1px solid ${(p) => p.theme.detailBlockBorder};
  border-bottom-left-radius: 16px;
  border-bottom-right-radius: 16px;
`;

const NotificationContainer = styled.div`
  width: 100%;
  margin: 20px 0 0;
`;

interface IFarmingStakeModal {
  open: boolean;
  handleOnClose: () => void;
  userFarming: IUserFarming;
}

export const FarmingStakeModal = ({
  open,
  handleOnClose,
  userFarming,
}: IFarmingStakeModal) => {
  const { mutateAsync: farmingStake } = useFarmingStake();
  const { mutateAsync: farmingWithdraw } = useFarmingWithdraw();
  const { currentUser } = useAuthContext();
  const { data: userLiquidity } = useUserLiquidity(currentUser?.actor);
  const theme = useTheme();

  const [value, setValue] = useState<string>("0");
  const [amountError, setAmountError] = useState<boolean>(false);

  const tokenPrecision =
    liquidityTokenPrecisions[process.env.NEXT_PUBLIC_ENV][userFarming.symbol];
  const maxAmount =
    userLiquidity && userFarming
      ? Number(userLiquidity[userFarming.symbol] as keyof IUserPoolSupply) +
        Number(userFarming.total_staked)
      : 0;
  const stakeText = `You Stake ${
    liquidityTokenPairs[process.env.NEXT_PUBLIC_ENV][userFarming.symbol]
  }:`;

  const sliderStep = useMemo(() => {
    if (maxAmount < 0.001) return 0.00000001;
    return 1 / 10 ** 4;
  }, [maxAmount]);

  const handleSubmit = async () => {
    let submitValue;
    if (Number(value) == Number(userFarming.total_staked))
      return setAmountError(true);
    if (Number(value) < Number(userFarming.total_staked)) {
      submitValue = String(Number(userFarming.total_staked) - Number(value));
      await farmingWithdraw(
        `${formatRoundedDownTokenPrecision({
          value: Number(submitValue),
          precision: tokenPrecision,
        })} ${userFarming.symbol}`
      );
      return handleOnClose();
    }
    submitValue = String(Number(value) - Number(userFarming.total_staked));
    await farmingStake(
      `${formatRoundedDownTokenPrecision({
        value: Number(submitValue),
        precision: tokenPrecision,
      })} ${userFarming.symbol}`
    );
    handleOnClose();
  };

  const handleChange = (value: number) => {
    if (amountError) setAmountError(false);
    setValue(String(value));
  };

  useEffect(() => {
    if (!userFarming.total_staked) return;
    setValue(String(userFarming.total_staked));
  }, [userFarming.total_staked]);

  if (!open) return <></>;

  return (
    <AppModal title="Stake LP Tokens" show={true} onClose={handleOnClose}>
      <Content>
        <SliderContainer>
          <InputSlider
            text={stakeText}
            valueSymbol="PUSDT/PBTC"
            initialValue={Number(userFarming?.total_staked)}
            minText="0"
            min={0}
            maxText="MAX"
            max={maxAmount}
            step={sliderStep}
            handleUpdate={handleChange}
          />
        </SliderContainer>
        <BottomContainer>
          <MediumButton
            text="Stake Now"
            color={theme.buttonOrangeText}
            backgroundColor={theme.buttonOrange}
            onClick={handleSubmit}
          />
        </BottomContainer>
        {amountError && (
          <NotificationContainer>
            <Notification type="error" text="Please adjust the amount." />
          </NotificationContainer>
        )}
      </Content>
    </AppModal>
  );
};
