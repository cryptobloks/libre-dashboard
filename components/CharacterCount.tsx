import { useMemo } from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
`;

const Span = styled.span<{ marginUp?: number }>`
  color: ${(p) => p.theme.labelGrey};
  font-size: 12px;
  margin-top: -${(p) => p.marginUp}px;
  background: #ffffff;
  position: absolute;
  padding-left: 4px;
`;

export const CharacterCount = ({
  value,
  max,
  marginUp = 0,
}: {
  value: string;
  max: number;
  marginUp?: number;
}) => {
  const valueCount = useMemo(() => {
    return value.trim().length;
  }, [value]);
  return (
    <Wrapper>
      <Span marginUp={marginUp}>{`${valueCount}/${String(max)}`}</Span>
    </Wrapper>
  );
};
