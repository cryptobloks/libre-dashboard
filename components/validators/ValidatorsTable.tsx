import Link from "next/link";
import { useContext, useState } from "react";
import {
  AutoSizer,
  Column,
  Index,
  InfiniteLoader,
  Table,
  TableCellProps,
  TableHeaderProps,
} from "react-virtualized";
import styled, { useTheme } from "styled-components";
import { IValidators, IValidatorVote } from "../../models/Validators";
import { useAuthContext } from "../../providers/AuthProvider";
import { ConnectWalletContext } from "../../providers/ConnectWalletProvider";
import { SmallButton } from "../../uikit/Button";
import {
  HEADER_HEIGHT,
  ROW_HEIGHT,
  TableWrapper,
  useGetTableInfo,
} from "../../uikit/Table";
import { VoteModal } from "./VoteModal";

const Overflow = styled.div`
  display: flex;
  overflow: auto;
`;

const HeadItem = styled.span`
  display: flex;
  align-items: center;
  cursor: pointer;
  font-size: 12.8px;
  font-weight: 600;
  color: ${(p) => p.theme.tableHeader};
  position: relative;
  white-space: nowrap;
  text-transform: none;
`;

const CellItem = styled.span`
  font-size: 15px;
  color: black;
`;

interface IValidatorsTable {
  data: IValidators[];
  votes: IValidatorVote | undefined;
  isLoading: boolean;
}

const ValidatorsTable = ({ data, votes, isLoading }: IValidatorsTable) => {
  const theme = useTheme();
  const { handleOnOpen: handleLoginOpen } = useContext(ConnectWalletContext);
  const { currentUser } = useAuthContext();

  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [selectedProducer, setSelectedProducer] = useState<string>("");

  const { tableHeight, resolveRowStyle, noRowsRenderer } = useGetTableInfo({
    data,
    isLoading,
    rows: 10,
  });

  const handleVoteAction = async (producer: string) => {
    if (!currentUser || !currentUser.actor) {
      handleLoginOpen();
      return;
    }

    setSelectedProducer(producer);
    setModalOpen(true);
  };

  const isRowLoaded = ({ index }: Index) => !!data[index];

  const loadMoreRows = async () => {
    return;
  };

  const headerRenderer = ({ label, dataKey }: TableHeaderProps) => {
    if (dataKey === "action") {
      return (
        <HeadItem style={{ display: "flex", justifyContent: "end" }}>
          {label}
        </HeadItem>
      );
    }
    return <HeadItem>{label}</HeadItem>;
  };

  const cellRenderer = ({ cellData, dataKey, rowData }: TableCellProps) => {
    if (dataKey === "action") {
      if (votes && votes.votedFor) {
        if (rowData.name === votes.votedFor) {
          return (
            <div
              style={{
                display: "flex",
                justifyContent: "end",
              }}
            >
              <SmallButton
                onClick={() => { }}
                text="Voted"
                backgroundColor={theme.buttonOrange}
                color={theme.buttonOrangeText}
                disabled
              />
            </div>
          );
        }
        return (
          <div
            style={{
              display: "flex",
              justifyContent: "end",
            }}
          >
            <SmallButton
              onClick={() => handleVoteAction(rowData.name)}
              text="Vote"
              buttonStyle="empty"
              backgroundColor={theme.buttonGrey}
              color={theme.buttonGreyText}
              border={theme.buttonGrey}
            />
          </div>
        );
      }
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "end",
          }}
        >
          <SmallButton
            onClick={() => handleVoteAction(rowData.name)}
            text="Vote"
            buttonStyle="emptyGradient"
            backgroundColor={theme.buttonOrange}
          />
        </div>
      );
    }

    if (dataKey === "totalVotes") {
      return (
        <CellItem>
          {Number((cellData).toFixed(0)).toLocaleString("en-us")}
        </CellItem>
      );
    }

    if (dataKey === "name") {
      return (
        <CellItem>
          <Link href={rowData.url} passHref>
            <a
              target="_blank"
              rel="noreferrer"
              style={{ color: "#ff8c00", textDecoration: "underline" }}
            >
              {cellData}
            </a>
          </Link>
        </CellItem>
      );
    }
    return <CellItem>{cellData}</CellItem>;
  };

  return (
    <TableWrapper height={tableHeight}>
      <AutoSizer>
        {({ width, height }) => (
          <InfiniteLoader
            isRowLoaded={isRowLoaded}
            loadMoreRows={loadMoreRows}
            rowCount={1}
          >
            {() => (
              <Table
                width={width < 1000 ? 1000 : width}
                height={height}
                headerHeight={HEADER_HEIGHT}
                rowHeight={ROW_HEIGHT}
                rowCount={data.length}
                rowGetter={({ index }) => data[index]}
                rowStyle={({ index }) =>
                  resolveRowStyle(index + 1 === data.length)
                }
                overscanRowCount={5}
                noRowsRenderer={noRowsRenderer}
              >
                <Column
                  label="Rank"
                  dataKey={"rank"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  minWidth={50}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Validators"
                  dataKey={"name"}
                  flexGrow={1}
                  flexShrink={1}
                  width={150}
                  minWidth={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Location"
                  dataKey={"location"}
                  flexGrow={1}
                  flexShrink={1}
                  width={150}
                  minWidth={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Total Votes"
                  dataKey={"totalVotes"}
                  flexGrow={1}
                  flexShrink={1}
                  width={200}
                  minWidth={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Votes %"
                  dataKey={"percentage"}
                  flexGrow={1}
                  flexShrink={1}
                  width={150}
                  minWidth={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Action"
                  dataKey={"action"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  minWidth={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
              </Table>
            )}
          </InfiniteLoader>
        )}
      </AutoSizer>
      <VoteModal
        open={modalOpen}
        handleOnClose={() => setModalOpen(false)}
        producer={selectedProducer}
      />
    </TableWrapper>
  );
};

export default ValidatorsTable;
