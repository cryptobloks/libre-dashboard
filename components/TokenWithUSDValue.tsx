import { useMemo } from "react";
import styled, { css } from "styled-components";
import { IExchangeRates, useExchangeRates } from "../hooks/useExchangeRates";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const TokenRow = styled.div<{ link?: boolean }>`
  span {
    font-weight: 500;
    font-size: 15px;
    line-height: 24px;
    color: ${(p) => p.theme.titleText};
  }

  ${(p) =>
    p.link &&
    css`
      span {
        cursor: pointer;
        text-decoration: underline;
      }
    `}
`;

const USDRow = styled.div`
  line-height: 1;

  span {
    font-weight: 600;
    font-size: 12px;
    line-height: 16px;
    color: #b1b1b9;
  }
`;

interface ITokenWithUSDValue {
  value: string;
  tokenSymbol: string;
  link?: () => void;
  hideSymbol?: boolean;
}

export const TokenWithUSDValue = ({
  value,
  tokenSymbol,
  link,
  hideSymbol,
}: ITokenWithUSDValue) => {
  const { data } = useExchangeRates();

  const convertedValue = useMemo(() => {
    if (!data) return "--";
    if (!value || Number(value) == 0) return "--";

    const rate = data[tokenSymbol as keyof IExchangeRates];
    if (!rate) return "--";
    let parsedValue = value;
    if (typeof value === "number") {
      parsedValue = Number(value).toFixed(20);
    }
    const float = parseFloat(String(parsedValue).replace(/[^0-9-.]/g, ""));
    return Number(Number(float * rate).toFixed(2)).toLocaleString("en-US");
  }, [data, value, tokenSymbol]);

  if (!value || value == "0") {
    return (
      <Wrapper>
        <TokenRow>
          <span>--</span>
        </TokenRow>
      </Wrapper>
    );
  }
  return (
    <Wrapper>
      <TokenRow link={!!link} onClick={link ? link : () => {}}>
        <span>{`${value} ${hideSymbol ? "" : tokenSymbol}`}</span>
      </TokenRow>
      <USDRow>
        <span>${convertedValue}</span>
      </USDRow>
    </Wrapper>
  );
};
