import Image from "next/image";
import { useContext, useEffect, useState } from "react";
import styled, { css, useTheme } from "styled-components";
import { useRemoveLiquidity } from "../../hooks/queries/tokens/useRemoveLiquidity";
import useUserTokens from "../../hooks/queries/tokens/useUserTokens";
import { liquidityTokenPrecisions, tokenImgSources } from "../../models/Tokens";
import { useAuthContext } from "../../providers/AuthProvider";
import { ConnectWalletContext } from "../../providers/ConnectWalletProvider";
import { NotificationContext } from "../../providers/NotificationProvider";
import { MediumButton } from "../../uikit/Button";
import { IconPairs } from "../../uikit/IconPairs";
import { InputSlider } from "../../uikit/InputSlider";
import { Notification } from "../../uikit/Notification";
import { formatRoundedDownTokenPrecision } from "../../utils";

const DetailsBox = styled.div`
  width: 100%;
  padding: 20px 24px;
  margin-bottom: 20px;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
`;

const DetailsRow = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  padding: 8px 0;
`;

const DetailsText = styled.span<{ red?: boolean; green?: boolean }>`
  font-size: 14px;
  font-weight: 500;
  color: ${(p) => p.theme.detailBlockTitle};
  ${(p) =>
    p.red &&
    css`
      color: ${(p) => p.theme.detailBlockAccentRed};
    `};
  ${(p) =>
    p.green &&
    css`
      color: ${(p) => p.theme.detailBlockAccentGreen};
    `};
`;

const TableItem = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  border-top: solid 1px ${(p) => p.theme.detailBlockBorder};
`;

const TableRow = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 20px 0;
  cursor: pointer;
`;

const TableCol = styled.div`
  display: flex;
  width: 100%;
`;

const TableColLeft = styled.div`
  display: flex;
  align-items: center;
  margin-right: auto;
`;

const TableItemText = styled.span`
  display: inline-block;
  font-size: 16px;
  font-weight: 500;
  margin-left: 10px;
`;

const ButtonContainer = styled.div`
  display: flex;
  margin-top: 20px;
  button {
    width: 100%;
  }
`;

const SliderContainer = styled.div`
  display: flex;
  width: 100%;
  margin-top: 10px;
`;

const NotificationContainer = styled.div`
  width: 100%;
  margin: 20px 0 0;
`;

interface ILiquidityDetailsBoxItem {
  data: {
    poolName: string;
    pool1Quantity: number;
    pool2Quantity: number;
    pool1Symbol: string;
    pool2Symbol: string;
    supply: number;
    balance: number;
  };
}

export const LiquidityDetailsBoxItem = ({ data }: ILiquidityDetailsBoxItem) => {
  const theme = useTheme();
  const { handleOnOpen: handleNotification } = useContext(NotificationContext);
  const { mutateAsync: removeLiquidity } = useRemoveLiquidity();
  const { handleOnOpen: handleLogin } = useContext(ConnectWalletContext);
  const { currentUser } = useAuthContext();
  const { data: userTokens } = useUserTokens({
    accountName: currentUser.actor,
    notifyOnChangeProps: ["data"],
  });

  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [yourTokens, setYourTokens] = useState<string>("");
  const [yourLiquidity, setYourLiquidity] = useState<string>("");
  const [yourShare, setYourShare] = useState<string>("");
  const [resetSliderValue, setResetSliderValue] = useState<Date>(new Date());
  const [balanceError, setBalanceError] = useState<boolean>(false);

  const handleUpdate = (value: number) => {
    const percentDiff = (100 - value) / 100;
    const yourShare = data.balance / data.supply;
    const pool1Liquidity = yourShare * data.pool1Quantity;
    const pool2Liquidity = yourShare * data.pool2Quantity;

    const newYourTokens = data.balance - percentDiff * data.balance;
    const newPool1Liquidity = pool1Liquidity - percentDiff * pool1Liquidity;
    const newPool2Liquidity = pool2Liquidity - percentDiff * pool2Liquidity;
    const newYourShare = newYourTokens / data.supply;

    if (balanceError) setBalanceError(false);

    setYourTokens(String(newYourTokens.toFixed(8)));
    setYourLiquidity(
      `${newPool1Liquidity.toFixed(8)} ${
        data.pool1Symbol
      } / ${newPool2Liquidity.toFixed(8)} ${data.pool2Symbol}`
    );
    setYourShare(`${Number(newYourShare * 100).toFixed(2)}%`);
  };

  const handleSubmit = async () => {
    const orgYourShare = data.balance / data.supply;
    const orgPool1Liquidity = orgYourShare * data.pool1Quantity;
    const orgPool2Liquidity = orgYourShare * data.pool2Quantity;

    const asset1String = yourLiquidity.split(" / ")[0];
    const asset2String = yourLiquidity.split(" / ")[1];
    const asset1Value =
      (orgPool1Liquidity - Number(asset1String.split(" ")[0])) * (1 - 0.00012);
    const asset2Value =
      (orgPool2Liquidity - Number(asset2String.split(" ")[0])) * (1 - 0.00012);

    const token1 = userTokens?.find((a) => a.symbol === data.pool1Symbol);
    const token2 = userTokens?.find((a) => a.symbol === data.pool2Symbol);

    const pool = data.pool2Symbol === "LIBRE" ? "BTCLIB" : "BTCUSD";

    const toSellValue = formatRoundedDownTokenPrecision({
      value: Number(data.balance - Number(yourTokens)),
      precision: liquidityTokenPrecisions[process.env.NEXT_PUBLIC_ENV][pool],
    });
    const minAsset1Value = formatRoundedDownTokenPrecision({
      value: Number(asset1Value),
      precision: token1?.precision ?? 8,
    });
    const minAsset2Value = formatRoundedDownTokenPrecision({
      value: Number(asset2Value),
      precision: token2?.precision ?? 6,
    });

    const payload = {
      toSell: `${toSellValue} ${pool}`,
      minAsset1: `${minAsset1Value} ${data.pool1Symbol}`,
      minAsset2: `${minAsset2Value} ${data.pool2Symbol}`,
      tokenFrom: token1,
      tokenTo: token2,
    };

    const response = await removeLiquidity(payload);
    if (response && response.success) {
      setResetSliderValue(new Date());
      setTimeout(() => {
        handleNotification(response.transactionId);
      }, 1000);
    }
  };

  const renderButton = () => {
    if (
      !currentUser ||
      !currentUser.actor ||
      currentUser.permission !== "active"
    )
      return (
        <MediumButton
          text="Connect Wallet"
          color={theme.buttonOrangeText}
          backgroundColor={theme.buttonOrange}
          onClick={handleLogin}
        />
      );
    return (
      <MediumButton
        text="Adjust"
        color={theme.buttonOrangeText}
        backgroundColor={theme.buttonOrange}
        onClick={handleSubmit}
      />
    );
  };

  useEffect(() => {
    const yourShare = data.balance / data.supply;
    const pool1Liquidity = yourShare * data.pool1Quantity;
    const pool2Liquidity = yourShare * data.pool2Quantity;

    setYourShare(`${Number(yourShare * 100).toFixed(2)}%`);
    setYourLiquidity(
      `${pool1Liquidity.toFixed(8)} ${
        data.pool1Symbol
      } / ${pool2Liquidity.toFixed(8)} ${data.pool2Symbol}`
    );
    setYourTokens(String(data?.balance));
  }, [data]);

  return (
    <TableItem>
      <TableRow onClick={() => setIsOpen(!isOpen)}>
        <TableCol>
          <TableColLeft>
            <IconPairs
              imgSrc1={
                tokenImgSources[process.env.NEXT_PUBLIC_ENV][data.pool1Symbol]
              }
              imgSrc2={
                tokenImgSources[process.env.NEXT_PUBLIC_ENV][data.pool2Symbol]
              }
            />
            <TableItemText>{data.poolName}</TableItemText>
          </TableColLeft>
          <Image
            src={isOpen ? "/icons/chevron-up.svg" : "/icons/chevron-down.svg"}
            width={22}
            height={22}
          />
        </TableCol>
      </TableRow>
      {isOpen && (
        <DetailsBox>
          <DetailsRow>
            <DetailsText>Your LP Tokens:</DetailsText>
            <DetailsText green>{yourTokens}</DetailsText>
          </DetailsRow>
          <DetailsRow>
            <DetailsText>Your Liquidity:</DetailsText>
            <DetailsText green>{yourLiquidity}</DetailsText>
          </DetailsRow>
          <DetailsRow>
            <DetailsText>Total Liquidity:</DetailsText>
            <DetailsText>{`${data.pool1Quantity.toFixed(8)} ${
              data.pool1Symbol
            } / ${data.pool2Quantity.toFixed(8)} ${
              data.pool2Symbol
            }`}</DetailsText>
          </DetailsRow>
          <DetailsRow>
            <DetailsText>Your share:</DetailsText>
            <DetailsText green>{yourShare}</DetailsText>
          </DetailsRow>
          <SliderContainer>
            <InputSlider
              text=""
              valueSymbol="LIBRE"
              initialValue={100}
              minText="0%"
              min={0}
              maxText="100%"
              max={100}
              hideInput={true}
              resetValue={resetSliderValue}
              handleUpdate={handleUpdate}
            />
          </SliderContainer>

          <ButtonContainer>{renderButton()}</ButtonContainer>
          {balanceError && (
            <NotificationContainer>
              <Notification
                type="error"
                text="You have an insuffient balance."
              />
            </NotificationContainer>
          )}
        </DetailsBox>
      )}
    </TableItem>
  );
};
