import React from "react";
import {
  AutoSizer,
  Column,
  InfiniteLoader,
  Table,
  TableCellProps,
} from "react-virtualized";
import styled, { useTheme } from "styled-components";
import useLeaderboardReferrals from "../../hooks/queries/referrals/useLeaderboardReferrals";
import { useAuthContext } from "../../providers/AuthProvider";
import { HEADER_HEIGHT, ROW_HEIGHT } from "../../uikit/Table";

const Wrapper = styled.div`
  padding: 24px;
  height: 500px;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
  background-color: #fff;
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: #110f28;
  margin: 0 0 25px;
  display: inline-block;
`;

const TableContainer = styled.div`
  width: 100%;
  height: 100%;
`;

const HeadItem = styled.span`
  display: flex;
  align-items: center;
  cursor: pointer;
  font-size: 12.8px;
  font-weight: 600;
  color: ${(p) => p.theme.tableHeader};
  position: relative;
  white-space: nowrap;
  text-transform: none;
`;

const CellItem = styled.span`
  font-size: 15px;
  color: black;
`;

const ReferralLeaderboardTable = ({}) => {
  const theme = useTheme();
  const { currentUser } = useAuthContext();

  if (!currentUser) return <></>;

  const { data } = useLeaderboardReferrals();

  if (!data) return <></>;

  const isRowLoaded = ({ index }: { index: number }) => !!data[index];

  const loadMoreRows = async () => {
    return;
  };

  const cellRenderer = ({ cellData }: TableCellProps) => {
    return <CellItem>{cellData}</CellItem>;
  };

  return (
    <Wrapper>
      <Title>Referral Leaderboard</Title>
      <TableContainer>
        <AutoSizer>
          {({ width, height }) => (
            <InfiniteLoader
              isRowLoaded={isRowLoaded}
              loadMoreRows={loadMoreRows}
              rowCount={1}
            >
              {({ onRowsRendered, registerChild }) => (
                <Table
                  ref={registerChild}
                  width={width}
                  height={height}
                  headerHeight={HEADER_HEIGHT}
                  rowHeight={ROW_HEIGHT}
                  rowCount={data.length}
                  rowGetter={({ index }) => data[index]}
                  rowStyle={{
                    borderBottom: `1px solid ${theme.detailBlockBorder}`,
                  }}
                  onRowsRendered={onRowsRendered}
                  overscanRowCount={5}
                >
                  <Column
                    label="Rank"
                    dataKey={"rank"}
                    flexGrow={1}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                  <Column
                    label="Libre name"
                    dataKey={"name"}
                    flexGrow={1}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                  <Column
                    label="People Joined"
                    dataKey={"referrees"}
                    flexGrow={1}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                </Table>
              )}
            </InfiniteLoader>
          )}
        </AutoSizer>
      </TableContainer>
    </Wrapper>
  );
};

export default ReferralLeaderboardTable;
