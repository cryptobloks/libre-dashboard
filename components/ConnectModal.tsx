import Image from "next/image";
import { useRouter } from "next/router";
import styled from "styled-components";
import { WalletType } from "../models/Client";
import { useAuthContext } from "../providers/AuthProvider";
import AppModal from "./AppModal";

const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const List = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 20px;
`;

const ListItem = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0.7rem 1rem;
  border: 1px solid ${(p) => p.theme.detailBlockBorder};
  border-radius: 10px;
  cursor: pointer;
  transition: 200ms all;

  :not(:last-of-type) {
    margin-bottom: 0.8rem;
  }

  &:hover {
    opacity: 0.8;
  }
`;

const ItemDiv = styled.div`
  display: flex;
  align-items: center;
`;

const ItemText = styled.span`
  margin-left: 1rem;
`;

interface IConnectModal {
  open: boolean;
  handleOnClose: () => void;
}

export const ConnectModal = ({ open, handleOnClose }: IConnectModal) => {
  const { login } = useAuthContext();
  const router = useRouter();

  const handleLogin = async (type: WalletType) => {
    await login({ type });
    if (router.pathname === "/[username]" || router.pathname === "/")
      router.push("/wallet");
    handleOnClose();
  };

  if (!open) return <></>;

  return (
    <AppModal title="Connect Wallet" show={true} onClose={handleOnClose}>
      <Content>
        <List>
          <ListItem onClick={() => handleLogin(WalletType.Libre)}>
            <ItemDiv>
              <Image
                src={"/icons/bitcoin-libre-icon.png"}
                height={32}
                width={33}
                style={{ borderRadius: 50 }}
              />
              <ItemText>Bitcoin Libre</ItemText>
            </ItemDiv>
          </ListItem>

          <ListItem onClick={() => handleLogin(WalletType.Anchor)}>
            <ItemDiv>
              <Image
                src={"/icons/anchor-wallet-icon.png"}
                height={33}
                width={33}
              />
              <ItemText>Anchor Wallet</ItemText>
            </ItemDiv>
          </ListItem>
        </List>
      </Content>
    </AppModal>
  );
};
