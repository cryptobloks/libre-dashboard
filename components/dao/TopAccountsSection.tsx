import { useRouter } from "next/router";
import styled from "styled-components";
import DaoEmptyState from "../../uikit/animations/dao-empty-state.json";
import { PageTitle } from "../PageTitle";
import { TopAccountsTable } from "./TopAccountsTable";
import { ViewMore } from "./ViewMore";

const Wrapper = styled.div`
  margin-top: 20px;
`;

const Block = styled.div`
  padding: 24px 24px 0;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
  background-color: #fff;
  margin-bottom: 50px;
  overflow: auto;
`;

const EmptyStateContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 100px 20px;
  text-align: center;
`;

const EmptyStateTitle = styled.div`
  font-size: 19.2px;
  font-weight: 600;
  margin-top: 15px;
  margin-bottom: 15px;
`;

const defaultOptions = {
  loop: false,
  autoplay: true,
  animationData: DaoEmptyState,
};

export const TopAccountsSection = () => {
  const router = useRouter();
  return (
    <Wrapper>
      <PageTitle title="Top Accounts by Voting Power" />
      <Block>
        <TopAccountsTable shortList />
        <ViewMore onClick={() => router.push("/dao/leaderboard")}>
          View Leaderboard
        </ViewMore>
      </Block>
    </Wrapper>
  );
};
