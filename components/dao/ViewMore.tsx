import styled, { css } from "styled-components";

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  padding: 20px 0;
  transform: 200ms all ease;

  &:hover {
    opacity: 0.8;
  }
`;

const Text = styled.span<{ vertical?: boolean; flip?: boolean }>`
  display: flex;
  align-items: center;
  position: relative;
  color: ${(p) => p.theme.titleText};
  width: 170px;

  &:after {
    content: "";
    position: absolute;
    right: -13px;
    top: 9px;
    transform: rotate(-45deg);
    border: solid ${(p) => p.theme.titleText};
    border-width: 0 2px 2px 0;
    padding: 3px;
    -webkit-transform-origin: 50% 53%;

    ${(p) =>
      p.vertical &&
      css`
        transform: rotate(45deg);
        top: 7px;
      `}

    ${(p) =>
      p.flip &&
      css`
        transform: rotate(225deg);
        top: 11px;
      `}
  }
`;

export const ViewMore = ({
  onClick,
  arrowVertical,
  flip,
  children,
}: {
  onClick: () => void;
  arrowVertical?: boolean;
  flip?: boolean;
  children: string;
}) => {
  return (
    <Wrapper onClick={onClick}>
      <Text vertical={arrowVertical} flip={flip}>
        {children}
      </Text>
    </Wrapper>
  );
};
