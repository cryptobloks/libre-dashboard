import Image from "next/image";
import styled from "styled-components";
import { useGovernanceOverviewData } from "../../hooks/dao/useGovernanceOverviewData";
import { DetailsBlock } from "../../uikit/DetailsBlock";
import { PageTitle } from "../PageTitle";

const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  column-gap: 2%;
  margin-bottom: 25px;
`;

export const GovernanceOverviewSection = () => {
  const { data, isLoading } = useGovernanceOverviewData();
  return (
    <>
      <PageTitle title="DAO Overview" />
      <Row>
        <DetailsBlock
          title="Total Voting Power"
          details={String(data.voting_power)}
          totalBlocks={2}
          loading={isLoading}
        />
        <DetailsBlock
          title="Total Value of DAO Treasury"
          details={`$${String(data.total_value)}`}
          totalBlocks={2}
          loading={isLoading}
        />
      </Row>
      <Row>
        <DetailsBlock
          title="Total LIBRE in DAO"
          details={String(data.libre)}
          image={
            <Image src="/icons/libre-asset-icon.svg" width={32} height={32} />
          }
          loading={isLoading}
        />
        <DetailsBlock
          title="Total pBTC in DAO"
          details={String(data.btc)}
          image={
            <Image src="/logos/bitcoin-circle.svg" width={32} height={32} />
          }
          loading={isLoading}
        />
        <DetailsBlock
          title="Total pUSDT in DAO"
          details={`$${String(data.usdt)}`}
          image={
            <Image src="/icons/usdt-asset-icon.png" width={34} height={32} />
          }
          loading={isLoading}
        />
      </Row>
    </>
  );
};
