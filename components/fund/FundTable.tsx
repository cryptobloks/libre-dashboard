import moment, { utc } from "moment";
import { useState } from "react";
import {
  AutoSizer,
  Column,
  InfiniteLoader,
  Table,
  TableCellProps,
} from "react-virtualized";
import styled, { useTheme } from "styled-components";
import { useClaimFundingStake } from "../../hooks/queries/funding/useClaimFundingStake";
import useUserFundingStats from "../../hooks/queries/funding/useUserFundingStats";
import { useAuthContext } from "../../providers/AuthProvider";
import { MediumButton } from "../../uikit/Button";
import {
  HEADER_HEIGHT,
  ROW_HEIGHT,
  TableWrapper,
  headerRenderer,
  useGetTableInfo,
} from "../../uikit/Table";
import { formatCurrencyWithPrecision } from "../../utils";
import { MediaQueryWidths } from "../../utils/constants";
import { EmptyContributions } from "../mint/EmptyContributions";
import { calculateAPYAndPayout } from "../stake/StakeModal";
import { FundModal } from "./FundModal";

const Wrapper = styled.div`
  padding: 24px;
  min-height: 500px;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
  background-color: #fff;
  margin-bottom: 40px;
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: #110f28;
  margin: 10px 0 25px;
  display: inline-block;
`;

const CellItem = styled.span`
  font-size: 15px;
  color: black;
`;

const Date = styled.span`
  display: flex;
  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: none;
  }
`;

const DateMobile = styled.span`
  display: none;
  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: inline-block;
  }
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media (max-width: ${MediaQueryWidths.small}px) {
    flex-direction: column;
    align-items: flex-start;
  }
`;

const HeaderCol = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 40px;
`;

const Btn = styled.div`
  display: flex;
  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: none;
  }
`;

const MobileButton = styled.div`
  display: none;

  button {
    min-width: 42px;
  }
  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: flex;
  }
`;

export const FundTable = ({}) => {
  const [modalOpen, setModalOpen] = useState<boolean>(false);

  const theme = useTheme();
  const { currentUser } = useAuthContext();
  let { data, isLoading } = useUserFundingStats(currentUser.actor);
  const { mutate: claimFundingStake } = useClaimFundingStake();

  const { resolveRowStyle, tableHeight } = useGetTableInfo({
    data: data?.contributions ?? [],
    isLoading,
    rows: 1,
  });

  const contributions = (data?.contributions || [])
    .map((contribution) => ({
      ...contribution,
      stakeDate: utc(contribution.stakeDate).local(),
      claimTime: utc(contribution.claimDate).local(),
      stakeApy: calculateAPYAndPayout(180, contribution.allocatedLibre),
    }))
    .filter((contribution) => contribution.status !== 4);

  const isRowLoaded = ({ index }: { index: number }) => !!contributions[index];

  const loadMoreRows = async () => {
    return;
  };

  const cellRenderer = ({ cellData, dataKey, rowData }: TableCellProps) => {
    if (dataKey === "contributionTime") {
      return (
        <CellItem>
          <Date>{moment(cellData).utc().local().format("MMM Do, YYYY")}</Date>
          <DateMobile>
            {moment(cellData).utc().local().format("MM/DD/YY")}
          </DateMobile>
        </CellItem>
      );
    }
    if (dataKey === "stakeDate") {
      return (
        <CellItem>
          <Date>{moment(cellData).format("MMM Do, YYYY")}</Date>
          <DateMobile>{moment(cellData).format("MM/DD/YY")}</DateMobile>
        </CellItem>
      );
    }
    if (dataKey === "claimTime") {
      if (rowData.status === 3) {
        return (
          <CellItem>
            <MediumButton
              color={theme.buttonOrangeText}
              backgroundColor={theme.buttonOrange}
              text="Claim"
              onClick={() => claimFundingStake(rowData.id)}
            />
          </CellItem>
        );
      }

      if (!rowData.claimDate) {
        return <CellItem>--</CellItem>;
      }
      return (
        <CellItem>
          <Date>{moment(cellData).format("MMM Do, YYYY")}</Date>
          <DateMobile>{moment(cellData).format("MM/DD/YY")}</DateMobile>
        </CellItem>
      );
    }
    if (dataKey === "contributedSats") {
      return (
        <CellItem>
          {formatCurrencyWithPrecision(Number(cellData), 0)} SATS
        </CellItem>
      );
    }
    if (dataKey === "stakeDays") {
      return <CellItem>{cellData} days</CellItem>;
    }
    if (dataKey === "allocatedLibre") {
      if (rowData.status === 0) {
        return <CellItem>Processing...</CellItem>;
      }
      return (
        <CellItem>
          {formatCurrencyWithPrecision(Number(cellData), 0)} LIBRE
        </CellItem>
      );
    }

    if (dataKey === "stakeApy") {
      return <CellItem>{Math.round(cellData.apy * 100)}%</CellItem>;
    }

    return <CellItem>{cellData}</CellItem>;
  };

  return (
    <Wrapper>
      <Header>
        <HeaderCol>
          <Title>My Contributions</Title>
        </HeaderCol>
        <HeaderCol>
          <Btn>
            <MediumButton
              color={theme.buttonOrangeText}
              backgroundColor={theme.buttonOrange}
              text="New Contribution"
              onClick={() => setModalOpen(!modalOpen)}
            />
          </Btn>
          <MobileButton>
            <MediumButton
              color={theme.buttonOrangeText}
              backgroundColor={theme.buttonOrange}
              text="New Contribution"
              onClick={() => setModalOpen(!modalOpen)}
            />
          </MobileButton>
        </HeaderCol>
      </Header>
      {!contributions || !contributions.length ? (
        <EmptyContributions noBorder />
      ) : (
        <TableWrapper height={tableHeight}>
          <AutoSizer>
            {({ width }) => (
              <InfiniteLoader
                isRowLoaded={isRowLoaded}
                loadMoreRows={loadMoreRows}
                rowCount={1}
              >
                {({ onRowsRendered, registerChild }) => (
                  <Table
                    ref={registerChild}
                    width={width < 900 ? 900 : width}
                    height={tableHeight}
                    headerHeight={HEADER_HEIGHT}
                    rowHeight={ROW_HEIGHT}
                    rowCount={contributions.length}
                    rowGetter={({ index }) => contributions[index]}
                    rowStyle={({ index }) =>
                      resolveRowStyle(index + 1 === contributions.length)
                    }
                    onRowsRendered={onRowsRendered}
                    overscanRowCount={5}
                  >
                    <Column
                      label="Contribution Date"
                      dataKey={"contributionTime"}
                      flexGrow={2}
                      flexShrink={1}
                      width={100}
                      headerRenderer={headerRenderer}
                      cellRenderer={cellRenderer}
                    />
                    <Column
                      label="SATS Contributed"
                      dataKey={"contributedSats"}
                      flexGrow={2}
                      flexShrink={1}
                      width={100}
                      headerRenderer={headerRenderer}
                      cellRenderer={cellRenderer}
                    />
                    <Column
                      label="Lock Up Period"
                      dataKey={"lockUpPeriod"}
                      flexGrow={2}
                      flexShrink={1}
                      width={100}
                      headerRenderer={headerRenderer}
                      cellRenderer={cellRenderer}
                    />
                    <Column
                      label="Allocation"
                      dataKey={"allocatedLibre"}
                      flexGrow={2}
                      flexShrink={1}
                      width={100}
                      headerRenderer={headerRenderer}
                      cellRenderer={cellRenderer}
                    />
                    <Column
                      label="Stake Date"
                      dataKey={"stakeDate"}
                      flexGrow={2}
                      flexShrink={1}
                      width={100}
                      headerRenderer={headerRenderer}
                      cellRenderer={cellRenderer}
                    />
                    <Column
                      label="Claim Date"
                      dataKey={"claimTime"}
                      flexGrow={2}
                      flexShrink={1}
                      width={100}
                      headerRenderer={headerRenderer}
                      cellRenderer={cellRenderer}
                    />
                  </Table>
                )}
              </InfiniteLoader>
            )}
          </AutoSizer>
        </TableWrapper>
      )}
      <FundModal open={modalOpen} handleOnClose={() => setModalOpen(false)} />
    </Wrapper>
  );
};
