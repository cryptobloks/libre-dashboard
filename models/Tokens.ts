import { CHAIN_CONFIG } from "../utils/constants";

export interface IToken {
  name: string;
  symbol: string;
  supply: number;
  marketCap: number | null;
  staked: number | null;
  apy: number | null;
  total: number;
  unstaked: number;
  precision: number;
  icon: string;
  amount: number;
}

export interface IUserToken {
  name: string;
  symbol: string;
  apy: number | null;
  enabled: boolean;
  staked: number;
  total: number;
  unstaked: number;
  precision: number;
  icon: string;
  total_staked_payout: number;
}

type Pool = {
  quantity: string;
  contract: string;
};
export interface ITokenInformation {
  fee: number;
  fee_contract: string;
  issuer: string;
  max_supply: string;
  pool1: Pool;
  pool2: Pool;
  supply: string;
}

export interface ITokenQuantities {
  BTCL: number;
  USDL: number;
}

export interface IPoolSupply {
  fee: number;
  fee_contract: string;
  issuer: string;
  max_supply: string;
  pool1: { quantity: string; contract: string };
  pool2: { quantity: string; contract: string };
  supply: string;
}

export interface IUserPoolSupply {
  BTCLIB: string;
  BTCUSD: string;
}

export interface IUserMintRush {
  claimed: string;
  daily_share: string;
  last_claim: string;
  mr_contrib_total: string;
  owner: string;
  unclaimed: string;
}

export interface ITransferToken {
  tokenName: string;
  to: string;
  from: string;
  quantity: string;
  memo: string;
}

export interface ITransferToAddress {
  from: string;
  memo: string;
  quantity: string;
  tokenContract: string;
  chainId: string;
}

export const liquidityTokenContracts = {
  testnet: {
    USDL: "eosio.token",
    BTCL: "eosio.token",
    LIBRE: "eosio.token",
  },
  mainnet: {
    PUSDT: "usdt.ptokens",
    PBTC: "btc.ptokens",
    LIBRE: "eosio.token",
  },
};

export const liquidityTokenPrecisions = {
  testnet: {
    BTCUSD: 7,
    BTCLIB: 6,
  },
  mainnet: {
    BTCUSD: 9,
    BTCLIB: 6,
  },
};

export const liquidityTokenPairs = {
  testnet: {
    BTCUSD: "BTCL/USDL",
    BTCLIB: "BTCL/LIBRE",
  },
  mainnet: {
    BTCUSD: "PBTC/PUSDT",
    BTCLIB: "PBTC/LIBRE",
  },
};

export const tokenImgSources = {
  testnet: {
    BTCL: "/icons/btc-asset-icon.svg",
    LIBRE: "/icons/libre-asset-icon.svg",
    USDL: "/icons/usdt-asset-icon.png",
  },
  mainnet: {
    PBTC: "/icons/btc-asset-icon.svg",
    LIBRE: "/icons/libre-asset-icon.svg",
    PUSDT: "/icons/usdt-asset-icon.png",
  },
};

export const emptyTokenFrom = {
  apy: 0,
  enabled: true,
  name: CHAIN_CONFIG().btc.symbol,
  staked: 0,
  symbol: CHAIN_CONFIG().btc.symbol,
  total: 0,
  unstaked: 0,
  precision: 0,
  icon: "",
  total_staked_payout: 0,
};

export const emptyTokenTo = {
  apy: 0,
  enabled: true,
  name: CHAIN_CONFIG().usdt.symbol,
  staked: 0,
  symbol: CHAIN_CONFIG().usdt.symbol,
  total: 0,
  unstaked: 0,
  precision: 0,
  icon: "",
  total_staked_payout: 0,
};

export const displayTokenPrecisions = {
  PUSDT: 2,
  USDL: 2,
  PBTC: 8,
  BTCL: 8,
  LIBRE: 0,
  BTCUSD: 9,
  BTCLIB: 6,
};
